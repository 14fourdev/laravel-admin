[![Creative Commons License](https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-nd/4.0/)  

# Laravel Admin

Laravel Admin is a package to assist in setting up and running Laravel for 14Four.

# Docs

See [Wiki](https://bitbucket.org/14fourdev/laravel-admin/wiki/Home) for installation and other documentation.


## License
[![Creative Commons License](https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-nd/4.0/)  
<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Laravel Adim</span> by [14Four](http://14four.com) is licensed under a [Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](http://creativecommons.org/licenses/by-nc-nd/4.0/).  
Based on a work at [https://bitbucket.org/14fourdev/laravel-admin](https://bitbucket.org/14fourdev/laravel-admin).