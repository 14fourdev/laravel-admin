<?php

use Orchestra\Testbench\TestCase;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TestAdminInit extends TestCase
{

    /**
    * Setup the test environment.
    */
    public function setUp()
    {
        parent::setUp();

        $this->artisan('admin:init');
    }


    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'testing');
    }

    //
    protected function getPackageProviders($app)
    {
        return [
            'Admin\AdminServiceProvider',
        ];
    }

    /**
     * @author Josh Hagel
     */
    public function test_database_migrations()
    {

        // $this->assertTrue(file_exists(database_path('/migrations/2015_01_01_000001_create_sessions_table.php')));
    }
}
