<?php namespace Tests\App\Console\Commands;

use Tests\BaseTestCase;
use DB;
use File;

/**
 * Admin Make Model Test
 */
class AdminMakeModelTest extends BaseTestCase
{
    public function tearDown()
    {
        if (file_exists(app_path('Models'))) {
            File::deleteDirectory(app_path('Models'));
        }

        parent::tearDown();
    }

    /**
     * Test The Make Model Command
     */
    public function testMakeModel()
    {
        $this->assertEmpty(DB::table('tests')->get());

        $file = app_path('Models') . '/Test.php';

        // $this->assertFalse(file_exists($file));

        $this->artisan('admin:make:model', [
            'name' => 'Test',
        ]);

        $this->assertTrue(file_exists($file));

        $this->assertFileEquals($file, $this->test_path() . '/resources/stubs/models/model.php');
    }


    public function testMakeSoftdeletesModel()
    {
        $this->assertEmpty(DB::table('softdeletes')->get());

        $file = app_path('Models') . '/Softdelete.php';

        // $this->assertFalse(file_exists($file));

        $this->artisan('admin:make:model', [
            'name' => 'Softdelete',
        ]);

        $this->assertTrue(file_exists($file));

        $this->assertFileEquals($file, $this->test_path() . '/resources/stubs/models/model-softdeletes.php');
    }
}
