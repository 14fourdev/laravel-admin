<?php namespace App\Models;

use Admin\Traits\Models\Api;
use Admin\Traits\Models\Dynamic;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Test extends Model
{

    /**
     * Include Api Trait so it's available to the model
     */
    use Api;

    /**
     * The Table Name inside the database
     *
     * @var string
     */
    protected $table = 'tests';

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'count',

    ];

    /**
     * The attributes that can be searched using the API Trait
     *
     * @var array
     */
    protected $searchable = [
        'name',
        'count',

    ];


    ////////////
    // SCOPES //
    ////////////


    ////////////
    // EVENTS //
    ////////////


    ///////////////////
    // RELATIONSHIPS //
    ///////////////////


}
