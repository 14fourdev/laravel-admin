<?php namespace Tests;

use Orchestra\Testbench\TestCase;
// use Illuminate\Routing\Router;
// use Illuminate\Foundation\Testing\WithoutMiddleware;
// use Illuminate\Foundation\Testing\DatabaseTransactions;
use Carbon\Carbon;
use DB;

class BaseTestCase extends TestCase
{

    /**
     * Retrun the base testing direcotry
     * @return string Base Testing Directory
     */
    public function test_path()
    {
        return __DIR__;
    }

    /**
    * Setup the test environment.
    */
    protected function setUp()
    {
        parent::setUp();

        $this->loadLaravelMigrations([]);

        $this->loadMigrationsFrom([
            '--database' => 'testbench',
            '--realpath' => realpath(__DIR__ . '/../src/database/migrations'),
        ]);

        $this->loadMigrationsFrom([
            '--database' => 'testbench',
            '--realpath' => realpath(__DIR__ . '/migrations'),
        ]);
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
    }

    //
    protected function getPackageProviders($app)
    {
        return [
            \Admin\AdminServiceProvider::class,
            \Orchestra\Database\ConsoleServiceProvider::class,
        ];
    }

    protected function getPackageAliases($app)
    {
    }

    /**
     * Test running migration.
     *
     * @test
     */
    public function testRunningMigration()
    {
        // $now = Carbon::now();
        //
        // DB::table('users')->insert([
        //     'first_name'       => 'Orchestra',
        //     'last_name'       => 'Testbench',
        //     'email'      => 'hello@orchestraplatform.com',
        //     'password'   => \Hash::make('456'),
        //     'created_at' => $now,
        //     'updated_at' => $now,
        // ]);
        // $users = DB::table('users')->where('id', '=', 1)->first();
        // $this->assertEquals('hello@orchestraplatform.com', $users->email);
        // $this->assertTrue(\Hash::check('456', $users->password));
    }

    // public function tearDown()
    // {
    //     parent::tearDown();
    // }
}
