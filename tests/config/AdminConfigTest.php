<?php namespace Tests\App\Console\Commands;

use Tests\BaseTestCase;
use Artisan;
use Admin\Models\User;
use DB;
use File;

class AdminConfigTest extends BaseTestCase
{
    public function tearDown()
    {
        if (file_exists(config_path() . '/admin.php')) {
            File::delete(config_path() . '/admin.php');
        }

        parent::tearDown();
    }

    public function testUnMigratedConfig()
    {
        $this->assertEquals('admin', config('admin.uri'));
        $this->assertEquals('teal', config('admin.theme'));
        $this->assertEquals('Dashboard', config('admin.meta_title'));
        $this->assertFalse(config('admin.enable_registration'));
        $this->assertTrue(config('admin.enable_remember'));
        $this->assertEquals('http://14four.com', config('admin.legal_url'));
        $this->assertEquals('14Four', config('admin.legal_title'));

        $this->assertTrue(config('admin.account_lockout.enabled'));
        $this->assertEquals(5, config('admin.account_lockout.attempts'));
        $this->assertEquals(10, config('admin.account_lockout.reenable'));
    }


    public function testPublish()
    {
        $file = config_path() . '/admin.php';

        $this->assertFalse(file_exists($file));

        Artisan::call('vendor:publish', [
            '--tag' => 'config',
            '--provider' => 'Admin\AdminServiceProvider',
        ]);

        $this->assertTrue(file_exists($file));
    }
}
