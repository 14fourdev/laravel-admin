# Release Notes

## v2.0.0
- Removed Dynamic Views and Routes
- Added Named Routes
- Moved Generators to TWIG

## v1.1.19
### Added


### Changed
- AdminInit function `composer dump-autoload` and `php composer.phar dump-autoload` check.

### Fixed
- Documentation for Auth.php config
