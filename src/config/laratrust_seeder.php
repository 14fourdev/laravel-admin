<?php
return [
    'role_structure' => [
        'superadministrator' => [
            'admin.users' => 'c,r,u,d',
            'admin.acl' => 'c,r,u,d',
            'admin.reports' => 'r',
            'admin.tools' => 'r',
            'profile' => 'r,u',
        ],
        'administrator' => [
            'admin.users' => 'c,r,u,d',
            'admin.reports' => 'r',
            'profile' => 'r,u',
        ],
        'user' => [
            'profile' => 'r,u',
        ],
    ],
    'permission_structure' => [
    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
