<?php

return [

    'logs' => [
        'title' => 'Logs',
        'laravel' => 'Laravel Logs',
    ],

    'server' => [
        'title' => 'Server',
        'environment' => 'Laravel Environment',
        'configuration' => 'Configuration',
        'extensions' => 'Extensions',
    ],

    'info' => [
        'title' => 'PHP Info',
    ]

];
