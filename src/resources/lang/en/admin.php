<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Administrator Language Lines
    |--------------------------------------------------------------------------
    |
    | en - English
    |
    */
    'admin' => 'Admin',

    'new' => 'New',
    'edit' => 'Edit',
    'details' => 'Details',
    'delete' => 'Delete',
    'return' => 'Return',
    'archive' => 'Archive',
    'search' => 'Search',
    'clear' => 'Clear',
    'order' => 'Order',
    'default' => 'Default',
    'total' => 'Total',    
    'cancel' => 'Cancel',
    'create' => 'Create',
    'update' => 'Update',
    'return' => 'Return to list',
    'deleted' => 'Deleted',
    'saved' => 'Saved',
    'created' => 'Created',
    'take' => 'Per-Page',

    'messages' => [
        'not_authorized' => 'You\'re not authorized to view that page.',
        'deleted' => 'Deleted!',
    ]
];
