<?php

return [
    'email' => 'Email',

    /*
    |
    |
    |
    | Login Page
    */
    'welcome_message' => 'Login to access your dashboard.',
    'password' => 'Password',
    'password_verify' => 'Confirm Password',
    'remember' => 'Remember Me',
    'signin' => 'Sign In',
    'forgot' => 'I forgot my password',
    'register' => 'Register a new account',
    'failed' => 'Authentication failed.',
    'lockout' => 'Account Locked. Please try again later.',
    'logout' => 'You have been logged out.',

    /*
    |
    |
    |
    | Forgot Page
    */
    'forgot_message' => 'Reset your password!',
    'forgot_button' => 'Send Reset Link',
    'forgot_return' => 'Return To Login',
    'forgot_instructions' => 'Instructions for signing in have been emailed to you',
    'forgot_instructions_error' => 'An error occured sending your reminder email. Please try again. If this problem continues, please contact support.',
    'forgot_invalid' => 'Invalid email address. Please try again.',

    /*
    |
    |
    |
    | Reset Page
    */
    'reset_button' => 'Reset',
    'reset_message' => 'Reset Password?',
    'reset_invalid_password' => 'Invalid Password Reset link. Please try again.',
    'reset_password' => 'Your password has been reset. Please login to continue.',
    'reset_invalid_details' => 'Invalid details. Please try again.',
    'reset_invalid_password' => 'Invalid Password. Please try again.',
    'reset_invalid_user' => 'Invalid User. Please try again.',
    'reset_invalid_token' => 'Invalid Token. Please try again.',
];
