<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Esers Language Lines
    |--------------------------------------------------------------------------
    |
    | en - English
    |
    */
  'title' => 'Users',
  'columns' => [
    'name' => 'Name',
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'email' => 'Email',
        'password' => 'Password',
        'password_confirmation' => 'Confirm Password',
        'role' => 'Role',
        'created_at' => 'Created At',
        'updated_at' => 'Updated At',
  ],
    'messages' => [
        'password_complex' => 'Password must be between 6 and 32 characters and include at least 3 out 4 of uppercase letters, lowercase letters, numbers and special characters.',
        'password_notused' => 'Password must not be one of last five used passwords.',
        'leave_blank' => 'leave black to remain unchanged',
        'forgot_subject' => 'Reset your password',
        'forgot_sent' => 'Instructions for signing in have been emailed to the user',
        'forgot_error' => 'An error occured when sending the user instructions. Please try again',
        'not_found' => 'That user doesn\'t exsist.'
    ],
];
