<?php

return [
	
	'reset' => [
		'title' => 'Password Reset',
    'welcome' => 'Hi :name,',
    'question' => 'Can\'t remember your password?',
    'username' => 'Your username is: :email',
    'link' => 'Click this link to reset your password',
    'didnt_ask' => 'Didn\'t ask to reset your password?',
    'disregard' => 'If that\'s the case, you don\'t need to take any further action and can safely disregard this email.',
	]
];
