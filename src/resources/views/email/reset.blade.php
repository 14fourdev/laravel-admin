@extends('admin::layout.email')

@section('body')

  <h2>@lang('admin::email.title')</h2>

  <p>@lang('admin::email.welcome', ['name' => 'user'])</p>

  <p>@lang('admin::email.question')</p>

  <p>@lang('admin::email.username', ['email' => $email])</p>

  <p>{{ link_to('/' . Config::get('admin.uri') . '/reset/' . $token, trans('admin::email.link')) }}</p>

  <p>{{ url('/' . Config::get('admin.uri') . '/reset/' . $token) }}</p>

  <p><small><strong>@lang('admin::email.didnt_ask')</strong><br />
  @lang('admin::email.disregard')</small></p>

@stop
