@extends('admin::layout.site')

@section('body')


    <div class="container">
        <div class="c-header">
            <h2>@lang('admin::users.title')</h2>
        </div>


        <div class="card">
            <div class="card-header">
                <h2>@lang('admin::admin.details')</h2>
            </div>
            <div class="card-body">
              <table class="table">

                  <tr>
                      <th width="250">@lang('admin::users.columns.name')</th>
                      <td>{{ $user->first_name }} {{ $user->last_name }}</td>
                  </tr>
                  <tr>
                      <th>@lang('admin::users.columns.email')</th>
                      <td>{{ $user->email }}</td>
                  </tr>
                  <tr>
                      <th>@lang('admin::users.columns.role')</th>
                      <td>
                            @foreach( $user->roles as $role )
                                {{ $role->name }}<br />
                            @endforeach
                        </td>
                  </tr>
                  <tr>
                      <th>@lang('admin::users.columns.created_at')</th>
                      <td>{{ $user->created_at->toDayDateTimeString() }}</td>
                  </tr>
                  <tr>
                      <th>@lang('admin::users.columns.updated_at')</th>
                      <td>{{ $user->updated_at->toDayDateTimeString() }}</td>
                  </tr>

                  <tr>
                      <td colspan="2">
                          <a href="{!! url( '/' . Config::get('admin.uri') . '/users/' . $user->id . '/forgot' ) !!}" class="btn btn-info btn-sm">@lang('admin::auth.forgot_button')</a>
                          <a href="{!! url( '/' . Config::get('admin.uri') . '/users/' . $user->id . '/edit' ) !!}" class="btn btn-primary btn-sm">@lang('admin::admin.edit')</a></td>
                  </tr>

              </table>

            </div>
        </div>
    </div>

@stop
