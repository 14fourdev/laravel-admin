@extends('admin::layout.site')

@section('body')

    <div class="container">
        <div class="c-header">
            <h2>@lang('admin::users.title')</h2>
        </div>

        @include('admin::partials.alerts')

        <form action="{!! isset($user) ? url( '/' . Config::get('admin.uri') . '/users/' . $user->id ) : url( '/' . Config::get('admin.uri') . '/users' ) !!}" method="post" id="userForm" class="form-horizontal" role="form">

            @if( isset($user) )
                {{ method_field('PATCH') }}
            @endif

            <div class="card">
                <div class="card-header">
                    <h2>@lang('admin::admin.details')</h2>
                </div>
                <div class="card-body card-padding">

                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                    @if( isset($user) )

                        <input type="hidden" name="id" id="id" value="{{ $user->id }}" />

                    @endif

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="first_name">@lang('admin::users.columns.first_name')</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <input type="text" class="form-control required" name="first_name" id="first_name" value="{{ old('first_name', isset($user->first_name) ? $user->first_name : '') }}" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="last_name">@lang('admin::users.columns.last_name')</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <input type="text"  class="form-control required" name="last_name" id="last_name" value="{{ old('last_name', isset($user->last_name) ? $user->last_name : '') }}" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="email">@lang('admin::users.columns.email')</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <input type="text"  class="form-control required" name="email" id="email" value="{{ old('email', isset($user->email) ? $user->email : '') }}" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="password">@lang('admin::users.columns.password')</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <input type="password"  class="form-control required" name="password" id="password" />
                            </div>
                            @if ( isset($user) )
                                <small class="help-block">(@lang('admin::users.messages.leave_blank'))</small>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="password_confirmation">@lang('admin::users.columns.password_confirmation')</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <input type="password"  class="form-control required" name="password_confirmation" id="password_confirmation" />
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="role_id">@lang('admin::users.columns.role')</label>
                        <div class="col-sm-10">
                            <div class="fg-line">

                                @foreach( $roles as $role )
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" value="{{ $role->id }}" name="role[]" {!! (isset($user) && $user->roles->contains('id', $role->id)) ? "checked='checked'" : "" !!}>
                                            <i class="input-helper"></i>
                                            {{ $role->name }}
                                        </label>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12 text-right">
                            <button type="submit" class="btn btn-primary waves-effect">{!! (!empty($user->id) ? trans('admin::admin.update') : trans('admin::admin.create')) !!}</button>
                            <a href="{!! url( '/' . Config::get('admin.uri') . '/users' ) !!}" class="btn btn-danger waves-effect">@lang('admin::admin.cancel')</a>
                        </div>
                    </div>

                </div>
            </div>
        </form>

    </div>

@stop
