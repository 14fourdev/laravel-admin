@extends('admin::layout.site')

@section('body')

<div class="container">
    <div class="c-header">
        <h2>@lang('admin::users.title')</h2>
    </div>

    <a href="{!! url( '/' . Config::get('admin.uri') . '/users/create' ) !!}" class="btn btn-float btn-danger m-btn waves-effect waves-circle waves-float"><i class="zmdi zmdi-plus"></i></a>

    <div class="card ">
        <div class="card-header">
            <h2>Search</h2>
        </div>
        <div class="card-body card-padding">

            <form action="" method="GET" class="row" role="form">
                <div class="col-sm-5">
                    <div class="form-group fg-line">
                        <label class="sr-only" for="q">@lang('admin::admin.search')</label>
                        <input type="text" name="q" id="q" class="form-control input-sm" value="{{ Request::input('q') }}" placeholder="@lang('admin::admin.search')" />
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <div class="fg-line">
                            <div class="select">
                                <select name="sort" class="form-control input-sm">
                                    <option value="">@lang('admin::admin.order')</option>
                                    <option value="created_at" {!! Request::input('sort') == 'created_at' ? 'selected="selected"' : "" !!}>@lang('admin::users.columns.created_at')</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-primary btn-sm m-t-5 waves-effect"><i class="zmdi zmdi-search"></i> @lang('admin::admin.search')</button>
                    <a href="{!! url( '/' . Config::get('admin.uri') . '/users' ) !!}" style="" class="btn palette-Blue-Grey bg btn-sm m-t-5 m-l-10 waves-effect">@lang('admin::admin.clear')</a>
                </div>
            </form>
        </div>
        <div class="login-alert">
            @include('admin::partials.alerts')
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h2>Results</h2>
        </div>
        <!-- Main content -->
        <div class="card-body table-responsive">

            <table class="table">

                <tr>
                    <th>@lang('admin::users.columns.name')</th>
                    <th>@lang('admin::users.columns.email')</th>
                    <th>Permissions</th>
                    <th style="width:200px; text-align:center;">@lang('admin::users.columns.created_at')</th>
                    <th style="width:260px;"></th>
                </tr>

                @foreach($users as $obj)

                    <tr>
                        <td>
                            <a href="{!! url( '/' . Config::get('admin.uri') . '/users/' . $obj->id ) !!}">{{ $obj->first_name }} {{ $obj->last_name }}</a>
                        </td>
                        <td>
                            <a href="mailto:{{ $obj->email }}" title="Click to send an email to this user">{{ $obj->email }}</a>
                        </td>
                        <td>
                            @foreach ($obj->roles as $key => $value)
                                {{ $value->name }}<br />
                            @endforeach
                        </td>
                        <td style="text-align:center;">
                            {!! date('Y-m-d', strtotime($obj->created_at) ) !!}
                        </td>
                        <td style="text-align:center;">
                            <a href="{!! url( '/' . Config::get('admin.uri') . '/users/' . $obj->id ) !!}" class="btn btn-info btn-sm">@lang('admin::admin.details')</a>
                            <a href="{!! url( '/' . Config::get('admin.uri') . '/users/' . $obj->id . '/edit' ) !!}" class="btn btn-primary btn-sm m-l-10">@lang('admin::admin.edit')</a>

                            @if (Auth::user()->id != $obj->id)
                                <form action="{!! url( '/' . Config::get('admin.uri') . '/users/' . $obj->id ) !!}" method="post" class="deleteForm m-l-10">
                                    <?= csrf_field() ?>
                                    <?= method_field('DELETE') ?>
                                    <button type="submit" class="btn btn-danger confirmDelete">@lang('admin::admin.delete')</button>
                                </form>
                            @endif

                        </td>
                    </tr>

                @endforeach

            </table>

        </div>

        <div class="card-body card-padding">

            <p><strong>@lang('admin::admin.total'): {!! $count !!}</strong></p>

            <div class="box-tools pull-right">
                {!! $users->render() !!}
            </div>

        </div>
    </div>
</div>

@stop
