@extends('admin::layout.site')

@section('body')
    <?php $model = $pageDetails['model']; ?>

    <div class="container">
        <div class="c-header">
            <h2><?= $pageDetails['name'] ?></h2>
        </div>

        <a href="{!! url( '/' . Config::get('admin.uri') . '/' . $pageDetails['route'] . '/create' ) !!}" class="btn btn-float btn-danger m-btn waves-effect waves-circle waves-float"><i class="zmdi zmdi-plus"></i></a>

        <div class="card ">
            <div class="card-header">
                <h2>Search</h2>
            </div>
            <div class="card-body card-padding">

                <form action="" method="GET" class="row" role="form">
                    <div class="col-sm-5">
                        <div class="form-group fg-line">
                            <label class="sr-only" for="q">@lang('admin::admin.search')</label>
                            <input type="text" name="q" id="q" class="form-control input-sm" value="{{ Request::input('q') }}" placeholder="@lang('admin::admin.search')" />
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <div class="fg-line">
                                <div class="select">
                                    <select name="sort" class="form-control input-sm">
                                        <option value="">@lang('admin::admin.order')</option>
                                        <option value="created_at" {!! Request::input('sort') == 'created_at' ? 'selected="selected"' : "" !!}>@lang('admin::users.columns.created_at')</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <button type="submit" class="btn btn-primary btn-sm m-t-5 waves-effect"><i class="zmdi zmdi-search"></i> @lang('admin::admin.search')</button>
                    </div>
                    <div class="col-sm-1">
                        <a href="{!! url( '/' . Config::get('admin.uri') . '/' . $pageDetails['route'] ) !!}" style="" class="btn palette-Blue-Grey bg btn-sm m-t-5 waves-effect">@lang('admin::admin.clear')</a>
                    </div>
                </form>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h2>Results</h2>
            </div>
            <!-- Main content -->
            <div class="card-body table-responsive">

                <table class="table">
                    <thead>
                        <tr>
                            @foreach( $pageDetails['columns'] as $column )
                                <th>{{ $column->Field }}</th>
                            @endforeach
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $$model as $row )
                            <tr>
                                @foreach( $pageDetails['columns'] as $column )
                                    <?php
                                        $columnContent = $row[$column->Field];

                                        if ( Storage::exists($columnContent) ) {
                                            $url = Storage::url($columnContent);
                                            if ( preg_match("/(\.jpg|\.png|\.bmp)$/", $columnContent) ) {
                                                $columnContent = '<a href="' . $url . '" target="_blank"><img src="' . $url . '" width="100" /></a>';
                                            } else {
                                                $columnContent = '<a href="' . $url . '" target="_blank">' . $url . '</a>';
                                            }
                                        }
                                    ?>
                                    <td>{!! $columnContent !!}</td>
                                @endforeach

                                <td class="text-right" style="white-space: nowrap;">
                                    <a href="{!! url( '/' . Config::get('admin.uri') . '/' . $pageDetails['route'] . '/' . $row->id . '/edit' ) !!}" class="btn btn-primary btn-sm">@lang('admin::admin.edit')</a>
                                    <a href="{!! url( '/' . Config::get('admin.uri') . '/' . $pageDetails['route'] . '/' . $row->id ) !!}" class="btn btn-info btn-sm">@lang('admin::admin.details')</a>
                                    <form method="post" class="deleteForm">
                                        <a href="{!! url( '/' . Config::get('admin.uri') . '/' . $pageDetails['route'] . '/' . $row->id . '/delete' ) !!}" class="btn btn-danger btn-sm confirmDelete">@lang('admin::admin.delete')</a>
                                    </form>
                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>

            <div class="card-body card-padding">

                <p><strong>@lang('admin::admin.total'): {!! $count !!}</strong></p>

                <div class="box-tools pull-right">
                    {!! $$model->render() !!}
                </div>

            </div>
        </div>
    </div>


@stop
