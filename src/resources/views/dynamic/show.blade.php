@extends('admin::layout.site')

@section('body')
    <?php $model = $pageDetails['model']; ?>

    <div class="container">
        <div class="c-header">
            <h2>{{ $pageDetails['name'] }}</h2>
        </div>


        <div class="card">
            <div class="card-header">
                <h2>@lang('admin::admin.details')</h2>
            </div>
            <div class="card-body">
              <table class="table">

                  @foreach( $pageDetails['columns'] as $column )
                      <?php $field = $column->Field; ?>
                      <tr>
                          <td><strong>{{ ucwords(str_replace('_', ' ', $field)) }}</strong></td>
                          <td>{{ $$model->$field }}</td>
                      </tr>
                  @endforeach

                  <tr>
                      <td colspan="2">
                          <a href="{!! url( '/' . Config::get('admin.uri') . '/' . $pageDetails['route'] . '/' . $$model->id . '/edit' ) !!}" class="btn btn-primary btn-sm">@lang('admin::admin.edit')</a>
                          <form method="post" class="deleteForm">
                              <a href="{!! url( '/' . Config::get('admin.uri') . '/' . $pageDetails['route'] . '/' . $$model->id . '/delete' ) !!}" class="btn btn-danger btn-sm confirmDelete">@lang('admin::admin.delete')</a>
                          </form>
                      </td>
                  </tr>

              </table>

            </div>
        </div>
    </div>

@stop
