<?php
    $column = $column->Field;
?>
<div class="form-group">
    <label class="col-sm-2 control-label" for="{{ $column }}">{{ $column }}</label>
    <div class="col-sm-3">
        <input type="text" class="form-control required" name="{{ $column }}" id="{{ $column }}" value="{{ old($column, isset($model->$column) ? $model->$column : '') }}" />
    </div>
</div>
