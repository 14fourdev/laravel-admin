<?php
    $column = $column->Field;
?>
<div class="form-group">
    <label class="col-sm-2 control-label" for="{{ $column }}">{{ $column }}</label>
    <div class="col-sm-3">
        <textarea class="form-control required" name="{{ $column }}" id="{{ $column }}">{{ old($column, isset($model->$column) ? $model->$column : '') }}</textarea>
    </div>
</div>
