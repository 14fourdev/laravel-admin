@extends('admin::layout.site')

@section('body')
    <?php $model = $pageDetails['model']; ?>

    <div class="c-header">
        <h2>
            <?= $pageDetails['name'] ?>
        </h2>
    </div>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">

                <form action="{!! url( '/' . Config::get('admin.uri') . '/' . $pageDetails['route'] ) !!}" method="post" id="userForm" class="form-horizontal" role="form">
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                    @if(!empty($$model->id))
                        <input type="hidden" name="id" id="id" value="{{ $$model->id }}" />
                    @endif

                    <div class="card">
                        <div class="card-header">
                            @if(!empty($$model->id))
                                <h2>Update</h2>
                            @else
                                <h2>Create New</h2>
                            @endif
                        </div>
                        <div class="card-body card-padding">

                            @foreach($pageDetails['columns'] as $column)
                                <?php
                                if ( !in_array($column->Field, $pageDetails['info']->getFillable()) ) {
                                    continue;
                                }

                                switch( true ) {
                                    case stripos($column->Type,'varchar') !== false:
                                        ?>
                                        @include('admin::dynamic.partials.varchar', ['column' => $column, 'model' => $$model])
                                        <?php
                                        break;
                                    case stripos($column->Type,'text') !== false:
                                        ?>
                                        @include('admin::dynamic.partials.text', ['column' => $column, 'model' => $$model])
                                        <?php
                                        break;
                                    default:
                                        ?>
                                        @include('admin::dynamic.partials.varchar', ['column' => $column, 'model' => $$model])
                                        <?php
                                        break;
                                }

                                ?>
                            @endforeach

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-2">
                                    <button type="submit" class="btn btn-success btn-sm" />{!! (!empty($$model->id) ? trans('admin::admin.update') : trans('admin::admin.create')) !!}</button>
                                    <a href="{!! url( '/' . Config::get('admin.uri') . '/' . $pageDetails['route'] ) !!}" class="btn btn-danger btn-sm">@lang('admin::admin.cancel')</a>
                                </div>
                            </div>
                        </div>
                    </div>


                </form>
            </div>
        </div>

    </section>


@stop
