@extends('admin::layout.site')

@section('body')


    <div class="container">
        <div class="c-header">
            <h2>@lang('admin::tools.server.title')</h2>
        </div>

        <div class="card">
            <div class="card-header">
                <div class="h2">@lang('admin::tools.server.environment')</div>
            </div>
            <div class="card-body">
                <table class="table">
                    <tr>
                        <th>Environment</th>
                        <td style="width: 75%;">{{ App::environment() }}</td>
                    </tr>
                </table>
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                <div class="h2">@lang('admin::tools.server.configuration')</div>
            </div>
            <div class="card-body">
                <table class="table">

                    <tr>
                        <th>Date/Time</th>
                        <td style="width: 75%;">{{ date('r') }} ({{ date_default_timezone_get() }})</td>
                    </tr>
                    <tr>
                        <th align='left'>Hostname</th>
                        <td style="width: 75%;">{{ gethostname() }}</td>
                    </tr>
                    @if (function_exists('apache_get_version'))
                        <tr>
                            <th align='left'>Apache</th>
                            <td style="width: 75%;">{{ apache_get_version() }}</td>
                        </tr>
                    @endif
                    <tr>
                        <th align='left'>PHP</th>
                        <td style="width: 75%;">{{ phpversion() }}</td>
                    </tr>

                </table>
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                <div class="h2">@lang('admin::tools.server.extensions')</div>
            </div>
            <div class="card-body">
                <table class="table">

                    <tr>
                        <th align='left'>Curl</th>
                        <td style="width: 75%;">{{ $extensions['curl'] }}</td>
                    </tr>
                    <tr>
                        <th align='left'>GD</th>
                        <td style="width: 75%;">{{ $extensions['gd'] }}</td>
                    </tr>
                    <tr>
                        <th align='left'>JSON</th>
                        <td style="width: 75%;">{{ $extensions['json'] }}</td>
                    </tr>
                    <tr>
                        <th align='left'>PDO</th>
                        <td style="width: 75%;">{{ $extensions['pdo'] }}</td>
                    </tr>
                    <tr>
                        <th align='left'>MCrypt</th>
                        <td style="width: 75%;">{{ $extensions['mycrypt'] }}</td>
                    </tr>
                    <tr>
                        <th align='left'>MySQL</th>
                        <td style="width: 75%;">{{ $extensions['mysql'] }}</td>
                    </tr>
                    <tr>
                        <th align='left'>Rewrite (.htaccess)</th>
                        <td style="width: 75%;">{{ $extensions['mod_rewrite'] }}</td>
                    </tr>

                </table>
            </div>
        </div>


    </div>
@stop
