@extends('admin::layout.site')

@section('body')

    <div class="container">
        <div class="c-header">
            <h2>@lang('admin::tools.logs.title')</h2>
        </div>

        <div class="card">
            <div class="card-header">
                <h2>@lang('admin::tools.logs.laravel')</h2>
            </div>
            <div class="card-body">

                <table class="table">
                    <?php foreach($log as $arr): ?>

                      <tr>
                          <td>
                              <a href="#" class="toggleBody"><?php echo $arr['error'] ?></a>

                              <div class="logDetails" style="display:none">
                                  <?php foreach($arr['trace'] as $line): ?>
                                      <?php echo substr($line, 3); ?><br />
                                  <?php endforeach; ?>
                              </div>

                          </td>
                      </tr>

                    <?php endforeach; ?>

                </table>
            </div>
        </div>


    </div>

@stop
