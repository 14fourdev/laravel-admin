@extends('admin::layout.site')

@section('body')

    <div class="container">
        <div class="c-header">
            <h2>@lang('admin::permissions.title')</h2>
        </div>

        @include('admin::partials.alerts')

        <form action="{!! isset($permission) ? url( '/' . Config::get('admin.uri') . '/access-control/permissions/' . $permission->id ) : url( '/' . Config::get('admin.uri') . '/access-control/permissions' ) !!}" method="post" id="userForm" class="form-horizontal" role="form">

            @if( isset($permission) )
                {{ method_field('PATCH') }}
            @endif

            <div class="card">
                <div class="card-header">
                    <h2>@lang('admin::admin.details')</h2>
                </div>
                <div class="card-body card-padding">

                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                    @if( isset($permission) )

                        <input type="hidden" name="id" id="id" value="{{ $permission->id }}" />

                    @endif

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="name">@lang('admin::permissions.columns.name')</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <input type="text" class="form-control required" name="name" id="name" value="{{ old('name', isset($permission->name) ? $permission->name : '') }}" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="display_name">@lang('admin::permissions.columns.display_name')</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <input type="text"  class="form-control required" name="display_name" id="display_name" value="{{ old('display_name', isset($permission->display_name) ? $permission->display_name : '') }}" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="description">@lang('admin::permissions.columns.description')</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <textarea class="form-control" name="description" id="description" rows="5">{{ old('description', isset($permission->description) ? $permission->description : '') }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="role_id">@lang('admin::permissions.columns.roles')</label>
                        <div class="col-sm-10">
                            <div class="fg-line">

                                @foreach( $roles as $role )
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" value="{{ $role->id }}" name="roles[]" {!! (isset($permission) && $permission->roles->contains('id', $role->id)) ? "checked='checked'" : "" !!}>
                                            <i class="input-helper"></i>
                                            {{ $role->display_name }}
                                        </label>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-2">
                            <button type="submit" class="btn btn-primary waves-effect">{!! (!empty($permission->id) ? trans('admin::admin.update') : trans('admin::admin.create')) !!}</button>
                            <a href="{!! url( '/' . Config::get('admin.uri') . '/access-control/permissions' ) !!}" class="btn palette-Blue-Grey bg m-l-10 waves-effect">@lang('admin::admin.cancel')</a>
                        </div>
                    </div>

                </div>
            </div>
        </form>

    </div>

@stop
