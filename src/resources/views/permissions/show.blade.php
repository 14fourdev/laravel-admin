@extends('admin::layout.site')

@section('body')


    <div class="container">
        <div class="c-header">
            <h2>@lang('admin::permissions.title')</h2>
        </div>


        <div class="card">
            <div class="card-header">
                <h2>@lang('admin::admin.details')</h2>
            </div>
            <div class="card-body">
                <table class="table">

                    <tr>
                        <th width="250">@lang('admin::permissions.columns.name')</th>
                        <td>{{ $permission->name }}</td>
                    </tr>
                    <tr>
                        <th>@lang('admin::permissions.columns.display_name')</th>
                        <td>{{ $permission->display_name }}</td>
                    </tr>
                    <tr>
                        <th>@lang('admin::permissions.columns.description')</th>
                        <td>{{ $permission->description }}</td>
                    </tr>
                    <tr>
                        <th>@lang('admin::permissions.columns.roles')</th>
                        <td>
                              @foreach( $permission->roles as $role )
                                  {{ $role->display_name }}<br />
                              @endforeach
                          </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <a href="{!! route('admin.permissions.edit', ['id' => $permission->id]) !!}" class="btn btn-primary btn-sm">@lang('admin::admin.edit')</a>
                            <form action="{!! route('admin.permissions.destroy', ['id' => $permission->id]) !!}" method="post" class="deleteForm">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-danger confirmDelete btn-sm">@lang('admin::admin.delete')</button>
                            </form>
                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>

@stop
