@extends('admin::layout.site')

@section('body')


    <div class="container">
        <div class="c-header">
            <h2>@lang('admin::roles.title')</h2>
        </div>


        <div class="card">
            <div class="card-header">
                <h2>@lang('admin::admin.details')</h2>
            </div>
            <div class="card-body">
                <table class="table">

                    <tr>
                        <th width="250">@lang('admin::roles.columns.name')</th>
                        <td>{{ $role->name }}</td>
                    </tr>
                    <tr>
                        <th>@lang('admin::roles.columns.display_name')</th>
                        <td>{{ $role->display_name }}</td>
                    </tr>
                    <tr>
                        <th>@lang('admin::roles.columns.description')</th>
                        <td>{{ $role->description }}</td>
                    </tr>
                    <tr>
                        <th>@lang('admin::roles.columns.permissions')</th>
                        <td>
                              @foreach( $role->permissions as $permission )
                                  {{ $permission->display_name }}<br />
                              @endforeach
                          </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <a href="{!! route('admin.roles.edit', ['id' => $role->id]) !!}" class="btn btn-primary btn-sm">@lang('admin::admin.edit')</a>
                            <form action="{!! route('admin.roles.destroy', ['id' => $role->id]) !!}" method="post" class="deleteForm">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-danger confirmDelete btn-sm">@lang('admin::admin.delete')</button>
                            </form>
                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>

@stop
