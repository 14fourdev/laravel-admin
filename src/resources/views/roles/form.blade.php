@extends('admin::layout.site')

@section('body')

    <div class="container">
        <div class="c-header">
            <h2>@lang('admin::roles.title')</h2>
        </div>

        @include('admin::partials.alerts')

        <form action="{!! isset($role) ? route('admin.roles.update', ['id' => $role->id]) : route('admin.roles.store') !!}" method="post" id="userForm" class="form-horizontal" role="form">

            @if( isset($role) )
                {{ method_field('PATCH') }}
            @endif

            <div class="card">
                <div class="card-header">
                    <h2>@lang('admin::admin.details')</h2>
                </div>
                <div class="card-body card-padding">

                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                    @if( isset($role) )

                        <input type="hidden" name="id" id="id" value="{{ $role->id }}" />

                    @endif

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="name">@lang('admin::roles.columns.name')</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <input type="text" class="form-control required" name="name" id="name" value="{{ old('name', isset($role->name) ? $role->name : '') }}" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="display_name">@lang('admin::roles.columns.display_name')</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <input type="text"  class="form-control required" name="display_name" id="display_name" value="{{ old('display_name', isset($role->display_name) ? $role->display_name : '') }}" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="description">@lang('admin::roles.columns.description')</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <textarea class="form-control" name="description" id="description" rows="5">{{ old('description', isset($role->description) ? $role->description : '') }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="role_id">@lang('admin::roles.columns.permissions')</label>
                        <div class="col-sm-10">
                            <div class="fg-line">

                                @foreach( $permissions as $permission )
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" value="{{ $permission->id }}" name="permissions[]" {!! (isset($role) && $role->permissions->contains('id', $permission->id)) ? "checked='checked'" : "" !!}>
                                            <i class="input-helper"></i>
                                            {{ $permission->name }}
                                        </label>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-2">
                            <button type="submit" class="btn btn-primary waves-effect">{!! (!empty($role->id) ? trans('admin::admin.update') : trans('admin::admin.create')) !!}</button>
                            <a href="{!! URL::previous() !!}" class="btn palette-Blue-Grey bg m-l-10 waves-effect">@lang('admin::admin.cancel')</a>
                        </div>
                    </div>

                </div>
            </div>
        </form>

    </div>

@stop
