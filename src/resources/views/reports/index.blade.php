@extends('admin::layout.site')

@section('body')

<?php
    if ($seven_days_difference > 0) {
        $seven = "increase";
    } else if ($seven_days_difference < 0) {
        $seven = "decrease";
    } else {
        $seven = "nochange";
    }

    if ($thirty_days_difference > 0) {
        $thirty = "increase";
    } else if ($thirty_days_difference < 0) {
        $thirty = "decrease";
    } else {
        $thirty = "nochange";
    }
?>


    <div class="container">
        <div class="c-header">
            <h2>{{ ucfirst($model) }} Report</h2>
        </div>


        <div class="row">

            <div class="col-md-4 col-sm-6 col-xs-12 reportWidgetItem">

                @include('admin::modules.progress', ['title' => 'Total', 'subtitle' => $total, 'data' => []])

            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 reportWidgetItem">

                <?php
                    if ( $seven == 'increase' ) {
                        $color = 'success';
                        $title = str_replace('-', '', $seven_days_difference) . '% Increase';
                    } elseif ( $seven == 'decrease') {
                        $color = 'danger';
                        $title = str_replace('-', '', $seven_days_difference) . '% Decrease';
                    } else {
                        $color = 'info';
                        $title = 'No Change';
                    }
                ?>
                @include('admin::modules.progress', ['title' => 'Last 7 days', 'subtitle' => $last_seven_days . ' new', 'data' => [['title' => $title, 'value' => $seven_days_difference, 'color' => $color]]])


            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 reportWidgetItem">

                <?php
                    if ( $thirty == 'increase' ) {
                        $color = 'success';
                        $title = str_replace('-', '', $thirty_days_difference) . '% Increase';
                    } elseif ( $thirty == 'decrease') {
                        $color = 'danger';
                        $title = str_replace('-', '', $thirty_days_difference) . '% Decrease';
                    } else {
                        $color = 'info';
                        $title = 'No Change';
                    }
                ?>
                @include('admin::modules.progress', ['title' => 'Last 30 days', 'subtitle' => $last_thirty_days . ' new', 'data' => [['title' => $title, 'value' => $thirty_days_difference, 'color' => $color]]])


            </div>

        </div>

        <div class="card">
            <div class="card-header">
                <h2>{{ ucfirst($model) }}</h2>
                <a href="{!! url( '/' . Config::get('admin.uri') . '/reports/'. $model . '/export' ) !!}" class="btn btn-success btn-sm pull-right">Download CSV</a>
            </div>
            <div class="card-body">
                <table class="table">

                    <thead>
                        <tr>
                            <th>By Week</th>
                            <th>Count</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($by_week as $obj)
                            <tr>
                                <th>{{ date('n/j/Y', strtotime($obj->year . 'W' . str_pad($obj->week, 2, '0', STR_PAD_LEFT)) ) }}</th>
                                <td style="width: 75%;">{{ $obj->count }}</td>
                            </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>


    </div>

@stop
