@extends('admin::layout.email')

@section('body')

  <h2>@lang('admin::email.reset.title')</h2>

  <p>@lang('admin::email.reset.welcome', ['name' => $user->first_name])</p>

  <p>@lang('admin::email.reset.question')</p>

  <p>@lang('admin::email.reset.username', ['email' => $user->email])</p>

  <p><a href="{!! url('/' . Config::get('admin.uri') . '/reset/' . $token) !!}" target="blank">@lang('admin::email.reset.link')</a></p>

  <p>{{ url('/' . Config::get('admin.uri') . '/reset/' . $token) }}</p>

  <p><small><strong>@lang('admin::email.reset.didnt_ask')</strong><br />
  @lang('admin::email.reset.disregard')</small></p>

@stop
