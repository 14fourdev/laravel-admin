<aside id="s-main-menu" class="sidebar">
    <div class="smm-header">
        <i class="zmdi zmdi-long-arrow-left" data-ma-action="sidebar-close"></i>
    </div>

    <ul class="smm-alerts">
        <li style="background:transparent;cursor:default">&nbsp;</li>
        {{-- <li data-user-alert="sua-messages" data-ma-action="sidebar-open" data-ma-target="user-alerts">
            <i class="zmdi zmdi-email"></i>
        </li>
        <li data-user-alert="sua-notifications" data-ma-action="sidebar-open" data-ma-target="user-alerts">
            <i class="zmdi zmdi-notifications"></i>
        </li>
        <li data-user-alert="sua-tasks" data-ma-action="sidebar-open" data-ma-target="user-alerts">
            <i class="zmdi zmdi-view-list-alt"></i>
        </li> --}}
    </ul>

    <ul class="main-menu">
        <li>
            <a href="{!! url( '/' . Config::get('admin.uri') ) !!}"><i class="zmdi zmdi-home"></i> Home</a>
        </li>

        @ability('superadministrator', 'admin.users.*')
            <li class="{!! Request::segment(2) == 'users' ? 'active' : ''; !!}"><a href="{!! url( '/' . Config::get('admin.uri') . '/users' ) !!}"><i class="zmdi zmdi-accounts-list"></i>Users</a></li>
        @endability

        @ability('superadministrator', 'admin.reports.*')
            <li class="sub-menu {!! in_array(Request::segment(3), ['users']) ? 'active' : '' !!}">
                <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-collection-item"></i> Reports</a>
                <ul>
                    <li class="{!! Request::segment(3) == 'users' ? 'active' : ''; !!}"><a href="{!! url( '/' . Config::get('admin.uri') . '/reports/users' ) !!}">Users</a></li>
                </ul>
            </li>
        @endability

        @ability('superadministrator', 'admin.acl.*')
            <li class="sub-menu {!! Request::segment(2) == 'access-control' ? 'active' : '' !!}">
                <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-lock"></i> Access Control</a>
                <ul>
                    <li class="{!! Request::segment(3) == 'roles' ? 'active' : ''; !!}"><a href="{!! url( '/' . Config::get('admin.uri') . '/access-control/roles' ) !!}">Roles</a></li>
                    <li class="{!! Request::segment(3) == 'permissions' ? 'active' : ''; !!}"><a href="{!! url( '/' . Config::get('admin.uri') . '/access-control/permissions' ) !!}">Permissions</a></li>
                </ul>
            </li>
        @endability

        @ability('superadministrator', 'admin.tools.*')
            <li class="sub-menu {!! in_array(Request::segment(3), ['logs', 'server', 'info']) ? 'active' : '' !!}">
                <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-menu"></i> Developers</a>
                <ul>
                    <li class="{!! Request::segment(3) == 'logs' ? 'active' : ''; !!}"><a href="{!! url( '/' . Config::get('admin.uri') . '/tools/logs' ) !!}">Logs</a></li>
                    <li class="{!! Request::segment(3) == 'server' ? 'active' : ''; !!}"><a href="{!! url( '/' . Config::get('admin.uri') . '/tools/server' ) !!}">Server</a></li>
                </ul>
            </li>
        @endability

    </ul>
</aside>
