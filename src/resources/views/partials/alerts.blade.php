@if (session('success'))

    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Success!</h4>
        @if( is_array(session('success')) )
            @foreach(session('success') as $message)
                {{ $message }}
            @endforeach
        @elseif( is_string(session('success')) )
            {{ session('success') }}
        @endif
    </div>

@endif

@if ( session('error') )

    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Error!</h4>
        @if( is_array(session('error')) )
            @foreach(session('error') as $message)
                {{ $message }}
            @endforeach
        @elseif( is_string(session('error')) )
            {{ session('error') }}
        @endif
    </div>

@endif

@if ( session('message') )

    <div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        @if( is_array(session('message')) )
            @foreach(session('message') as $message)
                {{ $message }}
            @endforeach
        @elseif( is_string(session('message')) )
            {{ session('message') }}
        @endif
    </div>

@endif

@if ( session('status') )

    <div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        @if( is_array(session('status')) )
            @foreach(session('status') as $status)
                {{ $status }}
            @endforeach
        @elseif( is_string(session('status')) )
            {{ session('status') }}
        @endif
    </div>

@endif

@if(isset($errors) && count($errors->all()) > 0)

    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Error!</h4>
        @foreach($errors->all() as $str)
            {{ $str }}<br />
        @endforeach
    </div>

@endif
