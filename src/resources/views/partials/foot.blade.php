<!-- Javascript Libraries -->
<script src="{{ asset('/vendor/admin/dist/vendors/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('/vendor/admin/dist/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendor/admin/dist/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('/vendor/admin/dist/vendors/bower_components/Waves/dist/waves.min.js') }}"></script>
<script src="{{ asset('/vendor/admin/dist/vendors/bootstrap-growl/bootstrap-growl.min.js') }}"></script>
<script src="{{ asset('/vendor/admin/dist/vendors/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('/vendor/admin/dist/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js') }}"></script>
<script src="{{ asset('/vendor/admin/dist/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js') }}"></script>
<script src="{{ asset('/vendor/admin/dist/vendors/bower_components/salvattore/dist/salvattore.min.js') }}"></script>

<script src="{{ asset('/vendor/admin/dist/vendors/bower_components/flot/jquery.flot.js') }}"></script>
<script src="{{ asset('/vendor/admin/dist/vendors/bower_components/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('/vendor/admin/dist/vendors/bower_components/flot.curvedlines/curvedLines.js') }}"></script>
<script src="{{ asset('/vendor/admin/dist/vendors/sparklines/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('/vendor/admin/dist/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
<script src="{{ asset('/vendor/admin/dist/js/flot-charts/curved-line-chart.js') }}"></script>
<script src="{{ asset('/vendor/admin/dist/js/flot-charts/line-chart.js') }}"></script>

<!-- Placeholder for IE9 -->
<!--[if IE 9 ]>
<script src="{{ asset('/vendor/admin/dist/vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js') }}"></script>
<![endif]-->

<script src="{{ asset('/vendor/admin/dist/js/charts.js') }}"></script>

<script src="{{ asset('/vendor/admin/dist/js/functions.js') }}"></script>
<script src="{{ asset('/vendor/admin/dist/js/actions.js') }}"></script>
<script src="{{ asset('/vendor/admin/custom/custom-scripts.js') }}"></script>
