<footer id="footer">
    Copyright &copy; {{ date('Y') }} {!! Config::get('admin.legal_title') !!}

    {{-- <ul class="f-menu">
        <li><a href="/">Home</a></li>
        <li><a href="">Dashboard</a></li>
        <li><a href="">Reports</a></li>
        <li><a href="">Support</a></li>
        <li><a href="">Contact</a></li>
    </ul> --}}
</footer>
