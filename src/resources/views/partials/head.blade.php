<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{!! Config::get('admin.meta_title') !!} - Admin</title>

<!-- Vendor CSS -->
<link href="{{ asset('/vendor/admin/dist/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css') }}" rel="stylesheet">
<link href="{{ asset('/vendor/admin/dist/vendors/bower_components/animate.css/animate.min.css') }}" rel="stylesheet">
<link href="{{ asset('/vendor/admin/dist/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css') }}" rel="stylesheet">
<link href="{{ asset('/vendor/admin/dist/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css') }}" rel="stylesheet">
<link href="{{ asset('/vendor/admin/dist/vendors/bower_components/google-material-color/dist/palette.css') }}" rel="stylesheet">
