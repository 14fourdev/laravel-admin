@extends('admin::layout.blank')

@section('body')

    <div class="l-block toggled" id="l-forget-password">
        <div class="lb-header palette-Purple bg">
            <i class="zmdi zmdi-account-circle"></i>
              @lang('admin::auth.reset_message')
          @include('admin::partials.alerts')
        </div>

        <div class="lb-body">

              <form action="{{ '/' . Config::get('admin.uri') . '/password/email' }}" method="post">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}" />

                    <p class="m-b-30"></p>

                    <div class="form-group fg-float">
                        <div class="fg-line">
                            <input type="email" name="email" class="input-sm form-control fg-input">
                            <label class="fg-label">@lang('admin::auth.email')</label>
                        </div>
                    </div>

                  <button class="btn palette-Purple bg">@lang('admin::auth.forgot_button')</button>
              </form>

            <div class="m-t-30">
                <a class="palette-Purple text d-block m-b-5" href="{{ '/' . Config::get('admin.uri') . '/login' }}">@lang('admin::auth.forgot_return')</a>
                  @if( Config::get('admin.enable_registration') )
                      <a class="palette-Teal text d-block m-b-5" href="{{ '/' . Config::get('admin.uri') . '/register' }}">@lang('admin::auth.register')</a>
                  @endif
            </div>
        </div>
    </div>

@endsection


@section('script')

  <script>
    $(function () {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
      });
    });
  </script>

@endsection
