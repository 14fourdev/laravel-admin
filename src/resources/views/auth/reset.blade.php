@extends('admin::layout.blank')

@section('body')

    <div class="l-block toggled" id="l-forget-password">
        <div class="lb-header palette-Purple bg">
            <i class="zmdi zmdi-account-circle"></i>
                @lang('admin::auth.reset_message')
            @include('admin::partials.alerts')
        </div>

        <div class="lb-body">

            <form action="{!! '/' . Config::get('admin.uri') . '/password/reset' !!}" method="post" role="form" autocomplete="off">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                <input type="hidden" name="token" value="{!! $token !!}">

                  <p class="m-b-30"></p>

                  <div class="form-group fg-float">
                      <div class="fg-line">
                          <input name="email" type="text" class="input-sm form-control fg-input">
                          <label class="fg-label">Email Address</label>
                      </div>
                  </div>

                  <div class="form-group fg-float">
                      <div class="fg-line">
                            <input id="password" type="password" class="input-sm form-control fg-input" name="password" required>
                            <label for="password" class="fg-label">{{ __('Password') }}</label>
                      </div>
                  </div>

                  <div class="form-group fg-float">
                      <div class="fg-line">
                            <input id="password-confirm" type="password" class="input-sm form-control fg-input" name="password_confirmation" required>
                            <label for="password-confirm" class="fg-label">{{ __('Confirm Password') }}</label>
                      </div>
                  </div>

                <button class="btn palette-Purple bg">@lang('admin::auth.reset_button')</button>
            </form>

          <div class="m-t-30">
              <a class="palette-Purple text d-block m-b-5" href="{{ '/' . Config::get('admin.uri') . '/login' }}">@lang('admin::auth.forgot_return')</a>
                @if( Config::get('admin.enable_registration') )
                    <a class="palette-Teal text d-block m-b-5" href="{{ '/' . Config::get('admin.uri') . '/register' }}">@lang('admin::auth.register')</a>
                @endif
          </div>
      </div>
  </div>

@stop
