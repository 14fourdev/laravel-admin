@extends('admin::layout.blank')

@section('body')

  <!-- Login -->
  <div class="l-block toggled" id="l-login">
      <div class="lb-header palette-Teal bg">
          <i class="zmdi zmdi-account-circle"></i>
          @lang('admin::auth.welcome_message')
      </div>

      <div class="lb-body">
            <form action="" method="post" autocomplete="off">
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                  <div class="form-group fg-float">
                      <div class="fg-line">
                          <input type="email" name="email" class="input-sm form-control fg-input">
                          <label class="fg-label">@lang('admin::auth.email')</label>
                      </div>
                  </div>

                  <div class="form-group fg-float">
                      <div class="fg-line">
                          <input type="password" name="password" class="input-sm form-control fg-input">
                          <label class="fg-label">@lang('admin::auth.password')</label>
                      </div>
                  </div>

                  <button class="btn palette-Teal bg">@lang('admin::auth.signin')</button>

                  <div class="m-t-20">
                        @if( Config::get('admin.enable_registration') )
                            <a class="palette-Teal text d-block m-b-5" href="{{ '/' . Config::get('admin.uri') . '/register' }}">@lang('admin::auth.register')</a>
                        @endif
                        <a href="{{ route('admin.password.request') }}" class="palette-Teal text">@lang('admin::auth.forgot')</a>
                  </div>
            </form>
      </div>
  </div>

  {{-- <!-- Register -->
  <div class="l-block" id="l-register">
      <div class="lb-header palette-Blue bg">
          <i class="zmdi zmdi-account-circle"></i>
          Create an account
      </div>

      <div class="lb-body">
          <div class="form-group fg-float">
              <div class="fg-line">
                  <input type="text" class="input-sm form-control fg-input">
                  <label class="fg-label">Name</label>
              </div>
          </div>

          <div class="form-group fg-float">
              <div class="fg-line">
                  <input type="text" class="input-sm form-control fg-input">
                  <label class="fg-label">Email Address</label>
              </div>
          </div>

          <div class="form-group fg-float">
              <div class="fg-line">
                  <input type="password" class="input-sm form-control fg-input">
                  <label class="fg-label">Password</label>
              </div>
          </div>

          <div class="checkbox m-b-30">
              <label>
                  <input type="checkbox" value="">
                  <i class="input-helper"></i>
                  Accept the license agreement
              </label>
          </div>

          <button class="btn palette-Blue bg">Create Account</button>

          <div class="m-t-30">
              <a data-block="#l-login" data-bg="{!! Config::get('admin.theme') !!}" class="palette-Blue text d-block m-b-5" href="">Already have an account?</a>
              <a data-block="#l-forget-password" data-bg="purple" href="" class="palette-Blue text">Forgot password?</a>
          </div>
      </div>
  </div> --}}

  {{-- <!-- Forgot Password -->
  <div class="l-block" id="l-forget-password">
      <div class="lb-header palette-Purple bg">
          <i class="zmdi zmdi-account-circle"></i>
          Forgot Password?
      </div>

      <div class="lb-body">
          <p class="m-b-30">Lorem ipsum dolor fringilla enim feugiat commodo sed ac lacus.</p>

          <div class="form-group fg-float">
              <div class="fg-line">
                  <input type="text" class="input-sm form-control fg-input">
                  <label class="fg-label">Email Address</label>
              </div>
          </div>

          <button class="btn palette-Purple bg">Create Account</button>

          <div class="m-t-30">
              <a data-block="#l-login" data-bg="{!! Config::get('admin.theme') !!}" class="palette-Purple text d-block m-b-5" href="">Already have an account?</a>
              <a data-block="#l-register" data-bg="blue" href="" class="palette-Purple text">Create an account</a>
          </div>
      </div>
  </div> --}}

@endsection

@section('script')

  <script>
    $(function () {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
      });
    });
  </script>

@endsection
