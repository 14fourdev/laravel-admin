@extends('admin::layout.site')

@section('body')
{{--
<section class="content-header">
    <h1>
        Dashboard
        <!-- <small>How many users have registered</small> -->
    </h1>
</section> --}}

<section class="content">

    <div class="row">

        <div class="col-md-6">

            @include('admin::modules.card', ['title' => 'Users', 'subTitle' => 'Registrations', 'count' => $usersTotal, 'data' => $userCount, 'labels' => $userLabels, 'palette' => null])

        </div>

    </div><!-- /.row -->

</section><!-- /.content -->

@stop
