<!DOCTYPE html>
    <!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        @include('admin::partials.head')
        @yield('styles')
        @include('admin::partials.custom-styles')
    </head>

    <body>
        <div class="login" data-lbg="{!! Config::get('admin.theme') !!}">

            @yield('body')

        </div>

        @include('admin::partials.ie')

        @include('admin::partials.foot')

        @push('script')

    </body>
</html>
