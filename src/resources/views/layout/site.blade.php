<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        @include('admin::partials.head')
        @yield('styles')
        @include('admin::partials.custom-styles')

    </head>
    <body data-ma-header="{!! Config::get('admin.theme') !!}">

        @include('admin::partials.header')

        <section id="main">

            @include('admin::partials.slideout')


            @include('admin::partials.sidebar')


            <section id="content">

                @include('admin::partials.alerts')

                @yield('child')

                @yield('body')

            </section>

            @include('admin::partials.footer')

        </section>

        @include('admin::partials.ie')

        @include('admin::partials.foot')

        @stack('script')

        <script src="{{ asset('/vendor/admin/dist/vendors/bower_components/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js') }}"></script>

        <script>
            $(document).ready(function () {
                // Delete functionality
                var deleteButtons = $('.confirmDelete');

                if(deleteButtons.length){
                    deleteButtons.on('click',function(e){
                        if(!confirm('Are you sure that you want to delete this item?')){
                            e.preventDefault();
                            return false;
                        }
                    });
                }


                // Date picker functionality
                var datetimePicker = $('.choose_datetime');

                if (datetimePicker.length) {
                    datetimePicker.each(function(){
                        $(this).datetimepicker({
                            format: 'MM/DD/YYYY'
                        });
                    });
                }

                @stack('inline-script')
            });
        </script>

    </body>
  </html>
