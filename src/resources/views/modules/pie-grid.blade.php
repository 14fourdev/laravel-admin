<!--
$palette
    Purple-300
    Light-Blue
    Teal-400
    Light-Green
    Teal
    Red-400
$data
-->


<!-- Pie Grid -->
<div class="card palette-{{ $palette or 'Red-400' }} bg">
    <div class="pie-grid clearfix text-center">
        @foreach ($data as $key => $value)
            <div class="col-xs-4 col-sm-6 col-md-4 pg-item">
                <div class="easy-pie-2 easy-pie" data-percent="{{ $value }}">
                    <span class="ep-value">{{ $value }}</span>
                </div>
                <div class="pgi-title">{{ $key }}</div>
            </div>
        @endforeach
    </div>
</div>
