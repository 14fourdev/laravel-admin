{{--
$title String
$subTitle String
$menu items
$count string
$data arrary
$labels array
$palette
    Purple-300
    Light-Blue
    Teal-400
    Light-Green
    Teal
    Red-400
--}}
<?php
    $id = str_random(10);
?>

<div class="card c-dark palette-{{$palette or 'Light-Blue'}} bg">
    <div class="card-header">
        <h2>{{ $title or '' }} <small>{{ $subTitle or '' }}</small></h2>

        <ul class="actions a-alt">
            <li class="dropdown">
                <a href="" data-toggle="dropdown">
                    <i class="zmdi zmdi-more-vert"></i>
                </a>

                {{-- <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <a href="">Change Date Range</a>
                    </li>
                    <li>
                        <a href="">Change Graph Type</a>
                    </li>
                    <li>
                        <a href="">Other Settings</a>
                    </li>
                </ul> --}}
            </li>
        </ul>
    </div>
    <div class="card-body card-padding">
        <h2 class="m-t-0 m-b-15 c-white">
        <i class="zmdi zmdi-caret-up-circle m-r-5"></i>
        {{ $count or '' }}
        </h2>

        <div class="sparkline-{{ $id }} text-center"></div>
    </div>
</div>

@push('inline-script')
    $('.sparkline-{{ $id }}').sparkline( {!! json_encode($data, JSON_UNESCAPED_SLASHES) !!}, {
        type: 'line',
        width: '100%',
        height: 50,
        lineColor: 'rgba(255,255,255,0.6)',
        fillColor: 'rgba(0,0,0,0)',
        lineWidth: 1.5,
        maxSpotColor: '#fff',
        minSpotColor: '#fff',
        spotColor: '#fff',
        spotRadius: 5,
        highlightSpotColor: '#fff',
        highlightLineColor: '#fff',
        {{-- {!! isset($labels) ? 'tooltipFormatFieldlist: ' . json_encode($labels, JSON_UNESCAPED_SLASHES) : '' !!} --}}
    });
@endpush
