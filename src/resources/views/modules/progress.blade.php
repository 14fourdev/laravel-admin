{{--
$title
$subtitle
$data = [
    [
        'title' => ''
        'description' => ''
        'value' => 0
        'color' => primary, info, danger, warning
    ]
]
--}}
<div class="card">
    <div class="card-header">
        <h2>{{ $title or '' }}
            @if( isset($subtitle) )
                <small>{{ $subtitle }}</small>
            @endif
        </h2>
    </div>

    <div class="card-body">
        <div class="list-group lg-alt">

            @foreach( $data as $item )

                <div class="list-group-item">
                    <div class="lgi-heading m-b-5">
                        {{ $item['title'] or '' }}
                        @if( isset($item['description']) )
                            <small>{{ $item['description'] }}</small>
                        @endif
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-{{ $color or 'primary'}}" role="progressbar" aria-valuenow="{{ $item['value'] or 0 }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $item['value'] or 0 }}%"></div>
                    </div>
                </div>

            @endforeach

        </div>
    </div>
</div>
