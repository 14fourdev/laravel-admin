<?php

namespace Admin\Console\Commands;

use Admin\Contracts\Generator;
use Admin\Traits\Console\Commands\ReadsTable;
use DB;

class AdminMakeModel extends GeneratorCommand implements Generator {

    use ReadsTable;

    /**
     * Exclude Items from the Fillable of a new Model.
     *
     * @var Array
     */
    private $protected = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];



    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:make:model {name} {--table=}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate new Model.';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->setModelData();

        $this->getTableName();

        $this->setFields();

        $this->generate();

    }




    public function getData() {

        return $this->data;

    }

    /**
     * [getTemplates description]
     * @return [type] [description]
     */
    public function getTemplates() {

        return [
            'Model.php.twig',
        ];

    }


    /**
     * [getOutputPath description]
     * @return [type] [description]
     */
    public function getOutputPath() {

        return app_path('Models');

    }


    /**
     * [getTemplatePaths description]
     * @return [type] [description]
     */
    public function getTemplatePaths() {

        return [
            __DIR__ . '/../../../resources/stubs/models/'
        ];

    }


    /**
     * [getOutputName description]
     * @param [type] $name [description]
     * @return [type] [description]
     */
    public function getOutputName( $name ) {

        return ucfirst($this->data['model']) . '.php';

    }


    /**
     * [setModelData description]
     */
    private function setModelData() {

        $this->data['model'] = $this->argument('name');

    }

    private function getTableName() {

        if ( $this->option('table') ) {
            $this->data['table'] =  $this->option('table');
        } else {
            $this->data['table'] = strtolower($this->argument('name') . 's');
        }

        return $this->data['table'];

    }


}
