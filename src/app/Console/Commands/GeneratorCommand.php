<?php

namespace Admin\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem as File;
use Admin\Builders\TwigBuilder;
use TwigGenerator\Builder\Generator;

abstract class GeneratorCommand extends Command {

    /**
     * Laravel Filesystem object
     *
     * @var Illuminate\Filesystem\Filesystem
     */
    public $file;

    public $templates = [];

    public $data = [];

    private $generator;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(File $file)
    {

        $this->file = $file;

        parent::__construct();

    }

    public function alreadyExsist( $name ) {

        return $this->file->exists( $this->getPath( $name ) );

    }

    public function generate() {

        $files = $this->getTemplates();

        $this->generator();

        foreach( $files as $file ) {

            $builder = $this->builder( $file );

            $this->generator->addBuilder( $builder );

        }

        $this->generator->writeOnDisk( $this->getOutputPath() );

    }

    /////////////
    // BUILDER //
    /////////////
    private function builder( $name ) {

        $builder = new TwigBuilder;

        $builder->setOutputName( $this->getOutputName( $name ) );

        $builder->setVariables( $this->data );

        $builder->setTemplateName( $name );

        return $builder;

    }

    ///////////////
    // GENERATOR //
    ///////////////
    private function generator() {

        $this->generator = new Generator;

        $this->generator->setTemplateDirs( $this->getTemplatePaths() );

        $this->generator->setMustOverwriteIfExists(true);

        $this->generator->setVariables( $this->data );

    }

}
