<?php

namespace Admin\Console\Commands;

use Admin\Contracts\Generator;
use DB;

class AdminMakeController extends GeneratorCommand implements Generator {

    private $protected = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:make:controller {model} {--name=} {--table=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate new Controller.';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->setModelData();

        $this->setClassName();

        $this->setFields();

        $this->generate();

    }

    /**
     * [setModelData description]
     */
    private function setModelData() {

        $this->data['model'] = $this->argument('model');

        $this->data['modelPlural'] = $this->data['model'] . 's';

    }


    /**
     * [getTemplates description]
     * @return [type] [description]
     */
    public function getTemplates() {

        return [
            'CrudController.php.twig',
        ];

    }


    /**
     * [getOutputPath description]
     * @return [type] [description]
     */
    public function getOutputPath() {

        return app_path('Http/Controllers/Admin/');

    }


    /**
     * [getTemplatePaths description]
     * @return [type] [description]
     */
    public function getTemplatePaths() {

        return [
            __DIR__ . '/../../../resources/stubs/controllers/'
        ];

    }


    /**
     * [getOutputName description]
     * @param [type] $name [description]
     * @return [type] [description]
     */
    public function getOutputName($name) {

        return $this->data['className'] . 'Controller.php';

    }


    public function getData() {

        return $this->data;

    }

    private function getTableName() {

        if ( $this->option('table') ) {
            return $this->option('table');
        }

        return strtolower($this->argument('model')) . 's';

    }

    private function setClassName() {

        if ( $this->option('name') ) {

            $cleanName = ucfirst(preg_replace('/Controller$/', '', $this->option('name')));

            $this->data['className'] = $cleanName;

        } else {

            $this->data['className'] = ucfirst( $this->argument('model') ) . 's';

        }

    }

    private function setFields() {

        $columns = DB::select('show full columns from ' . $this->getTableName() );

        $fields = [];

        foreach ( $columns as $column ) {

            $fields[] = [
                'name' => $column->Field,
                'required' => ($column->Null == 'NO' && $column->Default == null),
                'nullable' => $column->Null == 'YES',
                'protected' => in_array($column->Field, $this->protected),
                'type' => $this->decodeFieldType( $column->Type ),
            ];

        }

        $this->data['fields'] = $fields;

    }


    private function decodeFieldType( $type ) {

        switch (true) {
            case stristr('time', $type):
                return 'date';
                break;

            case stristr('date', $type):
                return 'date';
                break;

            case stristr('int', $type):
                return 'integer';
                break;

            default:
                return 'string';
                break;
        }

    }

}
