<?php

namespace Admin\Console\Commands;

use Illuminate\Console\Command;
use Admin\Helpers\StubProcess;
use Illuminate\Filesystem\Filesystem as File;
use App;
use Admin\Traits\Console\Commands\Notifies;

class AdminInit extends Command
{
    use Notifies;

    private $user = [
        'first_name' => 'Admin',
        'last_name' => 'User',
        'email' => 'admin@14four.com',
        'password' => 'password123',
    ];


    private $file;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initiate the 14Four Admin Package.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(File $file)
    {
        $this->file = $file;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // Check if the Environment is in Production
        // Promt User if they want to continue.
        if (App::environment('production')) {
            $this->titleComment('Application In Production!');

            if (!$this->confirm('You are in production, do you wish to continue? [y|N]')) {
                return;
            }
        }

        // Publish Admin
        $this->info('Publishing Files');

        $this->call('admin:make:user-model');

        $this->call('vendor:publish', [
            '--tag' => [
                'database',
                'public',
                'menu',
                'config',
            ],
            '--force' => true,
            '--provider' => 'Admin\AdminServiceProvider',
        ]);

        $this->setupDefaultAdminUser();

        // Configure Filesystem
        $this->setupFilesystem();

        $this->info('Clearing Compiled');
        $this->call('clear-compiled');

        if ($this->file->exists(app_path('composer.phar'))) {
            exec('php composer.phar dump-autoload');
        } else {
            exec('composer dump-autoload');
        }

        $this->info('Clearing Config');
        $this->call('config:clear');

        $this->info('Setup Laratrust');
        $this->call('laratrust:setup');
        $this->call('laratrust:seeder');

        $this->warn('Running Migrations');
        $this->call('migrate');

        sleep(30);

        $this->warn('Running Seeds');
        $this->call('db:seed');
    }


    public function setupDefaultAdminUser()
    {
        $this->titleComment('Setup the default Admin Users');

        $this->user['first_name'] = $this->ask('First Name');
        $this->user['last_name'] = $this->ask('Last Name');
        $this->user['email'] = $this->ask('Email');
        $this->user['password'] = $this->ask('password');

        $processor = new StubProcess($this->user, database_path('seeds'));

        $process = $processor->processFiles(__DIR__ . '/../../../resources/stubs/database/');

        return;
    }

    /**
     * Setup Fileystem
     * @return Void
     */
    private function setupFilesystem()
    {
        $this->comment('**************************************');
        $this->comment('*         Setup the FileSystem       *');
        $this->comment('**************************************');
        $this->setupSymbolicLink();
        $this->setDefaultFilesystemDriver();
    }

    /**
     * Setup A symbolic Link to the public folder from the storage directory
     * @return void
     */
    private function setupSymbolicLink()
    {
        if (!$this->file->exists(public_path('storage'))) {
            symlink(storage_path('app/public'), public_path('storage'));
        }
    }


    private function setDefaultFilesystemDriver()
    {
        if ($this->confirm('Do you wish to update the FileSystem Driver to "public"?', true)) {
            $finalPath = config_path('filesystems.php');

            $configContent = $this->file->get($finalPath);

            $finalContent = str_replace("'default' => 'local',", "'default' => 'public',", $configContent);

            $this->file->put($finalPath, $finalContent);
        }
    }
}
