<?php

namespace Admin\Console\Commands;

use Illuminate\Console\Command;
use Admin\Helpers\StubProcess;
use Illuminate\Filesystem\Filesystem as File;
use Symfony\Component\Finder\SplFileInfo;
use App;
use DB;

class AdminMakeCrud extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:make:crud {model} {--table=} {--controller=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Model, Controller, and Views for a table.';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->createModel();
        $this->createController();
        $this->createViews();
        $this->createLanguage();

    }

    private function createModel() {

        $data = [
            'name' => $this->argument('model')
        ];

        if ( $this->option('table') ) {
            $data['--table'] = $this->option('table');
        }

        $this->call('admin:make:model', $data);

    }

    private function createController() {

        $data = [
            'model' => $this->argument('model')
        ];

        if ( $this->option('controller') ) {
            $data['--name'] = $this->option('controller');
        }

        $this->call('admin:make:controller', $data);

    }

    private function createViews() {

        $this->call('admin:make:views', [
            'model' => $this->argument('model')
        ]);

    }

    private function createLanguage() {

        $this->call('admin:make:language', [
            'model' => $this->argument('model')
        ]);

    }

    private function createRoute() {

        // TODO Create Route Resourece for display

    }

}
