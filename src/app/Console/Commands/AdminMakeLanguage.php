<?php

namespace Admin\Console\Commands;

use Admin\Contracts\Generator;
use Admin\Traits\Console\Commands\ReadsTable;

class AdminMakeLanguage extends GeneratorCommand implements Generator {

    use ReadsTable;

    private $protected = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:make:language {model} {--name=} {--table=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate new language file.';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->setModelData();

        $this->setClassName();

        $this->setFields();

        $this->generate();

    }


    public function getTemplates() {

        return [
            'trans.php.twig',
        ];

    }


    public function getOutputPath() {

        return resource_path('lang/en');

    }


    public function getTemplatePaths() {

        return [
            __DIR__ . '/../../../resources/stubs/lang/en/'
        ];

    }

    public function getOutputName($name) {

        return 'admin' . ucfirst($this->data['modelPlural']) . '.php';

    }


    public function getData() {

        return $this->data;

    }

    private function getTableName() {

        if ( $this->option('table') ) {
            return $this->option('table');
        }

        return strtolower($this->argument('model')) . 's';

    }

    private function setModelData() {

        $this->data['model'] = $this->argument('model');

        $this->data['modelPlural'] = $this->data['model'] . 's';

    }

    private function setClassName() {

        if ( $this->option('name') ) {

            $cleanName = ucfirst(preg_replace('/Controller$/', '', $this->option('name')));

            $this->data['className'] = $cleanName;

        } else {

            $this->data['className'] = ucfirst( $this->argument('model') ) . 's';

        }

    }

}
