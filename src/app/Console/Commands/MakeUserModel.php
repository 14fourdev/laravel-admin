<?php

namespace Admin\Console\Commands;

use Admin\Contracts\Generator;
use Admin\Traits\Console\Commands\ReadsTable;
use DB;

class MakeUserModel extends GeneratorCommand implements Generator
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:make:user-model';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate new new User Model.';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $this->setFields();

        $this->generate();
    }




    public function getData()
    {
        return [];
    }

    /**
     * [getTemplates description]
     * @return [type] [description]
     */
    public function getTemplates()
    {
        return [
            'User.php.twig',
        ];
    }


    /**
     * [getOutputPath description]
     * @return [type] [description]
     */
    public function getOutputPath()
    {
        return app_path('Models');
    }


    /**
     * [getTemplatePaths description]
     * @return [type] [description]
     */
    public function getTemplatePaths()
    {
        return [
            __DIR__ . '/../../../resources/stubs/models/'
        ];
    }


    /**
     * [getOutputName description]
     * @param [type] $name [description]
     * @return [type] [description]
     */
    public function getOutputName($name)
    {
        return 'User.php';
    }
}
