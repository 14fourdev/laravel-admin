<?php

namespace Admin\Console\Commands;

use Illuminate\Console\Command;

class MakeDashboard extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:dashboard';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a new section in the dashboard.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $model = $this->ask('What is your Model Name?');

        if ($this->confirm('Do you wish to continue? [y|N]')) {

        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
      return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
      return [];
    }
}
