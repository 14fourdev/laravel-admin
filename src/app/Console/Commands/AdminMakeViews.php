<?php

namespace Admin\Console\Commands;

use Admin\Contracts\Generator;
use Admin\Traits\Console\Commands\ReadsTable;
use DB;

class AdminMakeViews extends GeneratorCommand implements Generator {

    use ReadsTable;

    private $protected = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:make:views {model} {--table=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate new Controller.';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->setModelData();

        $this->setFields();

        $this->getDisplayFields();

        $this->generate();

    }


    public function getTemplates() {

        return [
            'details.php.twig',
            'form.php.twig',
            'index.php.twig',
        ];

    }


    public function getOutputPath() {

        return resource_path('views/admin/' . $this->data['model'] . 's');

    }


    public function getTemplatePaths() {

        return [
            __DIR__ . '/../../../resources/stubs/views/'
        ];

    }

    public function getOutputName($name) {

        return str_replace('.php.twig', '.blade.php', $name);

    }


    public function getData() {

        return $this->data;

    }

    private function getTableName() {

        if ( $this->option('table') ) {
            return $this->option('table');
        }

        return strtolower($this->argument('model')) . 's';

    }

    private function setModelData() {

        $this->data['model'] = $this->argument('model');

        $this->data['modelPlural'] = $this->data['model'] . 's';

    }

    private function getDisplayFields() {

        $fieldNames = [];

        $fieldsSelected = [];

        foreach( $this->data['fields'] as $key => $field) {
            $fieldNames[] = $field['name'];
            if ( !$field['protected'] ) {
                $fieldsSelected[] = $key;
            }
        }

        $display = $this->choice('What columns should be displayed?', $fieldNames, implode(',', $fieldsSelected), 1, true);

        for( $i = 0; $i < count($this->data['fields']); $i++) {
            if ( in_array($this->data['fields'][$i]['name'], $display) ) {
                $this->data['fields'][$i]['display'] = true;
            } else {
                $this->data['fields'][$i]['display'] = false;
            }
        }

    }

}
