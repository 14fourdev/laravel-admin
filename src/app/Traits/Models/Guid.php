<?php
namespace Admin\Traits\Models;

/**
 * @author Josh Hagel <josh@14four.com>
 * @author Steve Wanless <steve@14four.com>
 * @since v1.2.0
 */
trait Guid {

    /**
     * Characters to be used in the generateRandomString Function
     *
     * @var string
     */
    public $guidCharacters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    /**
     * Generate a random string for a GUID
     *
     * @param integer $length = 8 Number of characters to generate the random string for
     * @return string A Randomly Generated GUID String
     */
    protected function generateRandomString( $length = 8 ) {

        return substr(str_shuffle($this->guidCharacters), 0, $length);

    }


    /**
     * Generate a GUID and check the database to make sure that it is actually unique.
     *
     * @param string  $field  [description]
     * @param integer $length [description]
     * @param string  $prefix [description]
     * @param boolean $verify [description]
     * @return string  [description]
     */
    public function guid( $field = 'guid', $length = 8, $prefix = null, $verify = true ) {

        $unique = false;

        $existing = 0;

        while ($unique == false) {

            $guid = ($prefix ? $prefix.'_' : '') . $this->generateRandomString($length);

            if ( $verify ) {
                $existing = $this->where($field, $guid)->count();
            }

            if ( $existing == 0 ) {
                $unique = true;
            }

        }

        return $guid;
    }

}
