<?php
namespace Admin\Traits\Models;

/**
 * @author Josh Hagel <josh@14four.com>
 * @since v1.2.0
 */
trait Api {

    private function apiSearchable() {

        $searchable = [];

        if ( $this->searchable ) {
            $searchable = $this->searchable;
        } else if ( $this->fillable ) {
            $searchable = $this->fillable;
        }

        return $searchable;

    }

    /**
     * Order the elements in the Model based on a set of query perams
     * @param Builder $query Current Builder for building the query
     * @param STRING $sort  Comma separated items that are to be sorted. Place '-' in front of item to denote DESC order.
     * @return Builder The modified Builder state
     */
    public function scopeOrder( $query, $sort = null ) {

        if ( $sort ) {

            $orders = explode(',', $sort);

            foreach( $orders as $order ) {
                $direction = 'ASC';

                if ( substr( $order, 0, 1 ) == '-' ) {
                    $direction = 'DESC';
                    $order = ltrim($order, '-');
                }

                $query->orderBy($order, $direction);

            }

        }

        return $query;

    }


    /**
     * Search the records in the model
     * @param Builder $query Current Builder for building the query
     * @param STRING $term  The term that the database should be searched on.
     * @return Builder The modified Builder state
     */
    public function scopeFilter( $query, $terms = [] ) {

        $searchable = $this->apiSearchable();

        $terms = array_diff( $terms, array( '' ) );

        // Move q to the end
        if ( isset($terms['q']) ) {
            $temp = $terms['q'];
            unset( $terms['q'] );
            $terms['q'] = $temp;
        }

        foreach( $terms as $key => $term ) {

            if ( $key == 'q') {

                $query->where( function($query) use ($searchable, $term) {

                    foreach ($searchable as $column) {
                        $query->orWhere($column, 'LIKE', "%$term%");
                    }

                });

            } else if ( in_array($key, $searchable) ) {

                if ( substr( $term, 0, 1 ) == '-' ) {
                    $query->where($key, '!=', ltrim($term, '-'));
                } else {
                    $query->where($key, $term);
                }

            }

        }

        return $query;

    }

}
