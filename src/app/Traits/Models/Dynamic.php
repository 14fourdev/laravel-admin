<?php
namespace Admin\Traits\Models;

/**
 * @author Josh Hagel <josh@14four.com>
 * @since v1.2.0
 */
trait Dynamic {

    public function getTableName() {
        return with(new static)->getTable();
    }

}
