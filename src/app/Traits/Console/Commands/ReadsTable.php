<?php

namespace Admin\Traits\Console\Commands;

use DB;

trait ReadsTable {

    /**
     * Get the Fields from a table
     */
    public function setFields() {

        $connection =  DB::connection();

        $prefix = $connection->getTablePrefix();

        $builder = $connection->getSchemaBuilder();

        $columns = $builder->getColumnListing( $prefix . $this->getTableName() );

        $fields = [];

        foreach ( $columns as $column ) {

            $columnType = $builder->getColumnType( $prefix . $this->getTableName(), $column );

            $fields[] = [
                'name' => $column,
                // 'required' => ($column->Null == 'NO' && $column->Default == null),
                // 'nullable' => $column->Null == 'YES',
                'protected' => in_array($column, $this->protected),
                // 'type' => $this->decodeFieldType( $columnType ),
            ];

        }

        $this->data['fields'] = $fields;

    }

    /**
     * Decode the field type
     * @param string $type type of filed
     * @return [type] [description]
     */
    public function decodeFieldType( $type ) {

        switch (true) {
            case stristr('time', $type):
                return 'date';
                break;

            case stristr('date', $type):
                return 'date';
                break;

            case stristr('int', $type):
                return 'integer';
                break;

            default:
                return 'string';
                break;
        }

    }

}
