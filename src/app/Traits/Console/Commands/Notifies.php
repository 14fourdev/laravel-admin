<?php
namespace Admin\Traits\Console\Commands;

trait Notifies {

    private function titleComment( $title ) {

        $comment = "*         {$title}       *";
        $count = strlen($comment);

        $this->comment( str_repeat('*', $count) );
        $this->comment( $comment );
        $this->comment( str_repeat('*', $count) );

    }


    private function descriptionComment() {

    }

}
