<?php

namespace Admin\Models;

use Admin\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Laratrust\Traits\LaratrustUserTrait;
use Admin\Traits\Models\Api;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Api, Authenticatable, CanResetPassword, Notifiable, LaratrustUserTrait;

    public static $rules = array(
        'id' => 'numeric',
        'email' => 'required|email',
        //'password' => 'complexpassword',
        'first_name' => 'required',
        'last_name' => 'required',
    );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_of_birth'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'attempts',
        'password',
        'used_passwords',
        'password_updated_at',
        'confirmed',
        'confirmation_code',
        'deleted_at',
        'remember_token',
    ];

    /**
     * The attributes that are not mass assignable
     *
     * @var array
     */
    protected $guarded = [
        'password'
    ];

    /**
     * The attributes that can be searched using the API Trait
     *
     * @var array
     */
    private $searchable = [
        'id',
        'first_name',
        'last_name',
        'email',
    ];

    /**
     * The attributes that will be added via custom getters
     *
     * @var array
     */
    protected $appends = [
    ];

    /**
     * Validation Error Messages
     *
     * @return array An Array of error messages per input.
     */
    public function messages()
    {
        return [
            'email.unique' => 'A user with that e-mail address already exists',
        ];
    }

    /**
     * Validation Rules
     *
     * @return array An array of validation rules based per input.
     */
    public function rules()
    {
        return [
            'id' => 'numeric',
            'email' => 'required|email|unique:users,email'.($this->id ? ",$this->id" : ''),
            'first_name' => 'required',
            'last_name' => 'required',
        ];
    }

    ////////////
    // EVENTS //
    ////////////

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->password_updated_at = date("Y-m-d H:i:s");
        });
    }

    /////////////////////////
    // GETTERS AND SETTERS //
    /////////////////////////

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
}
