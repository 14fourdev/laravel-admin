<?php
namespace Admin;

use Illuminate\Support\ServiceProvider;

/**
 * Laravel Service provider for the Admin Package.
 */
class AdminServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // REGISTER COMMANDS
        if ($this->app->runningInConsole()) {
            $this->commands([
                \Admin\Console\Commands\AdminMakeModel::class,
                \Admin\Console\Commands\MakeUserModel::class,
                \Admin\Console\Commands\AdminMakeController::class,
                \Admin\Console\Commands\AdminMakeViews::class,
                \Admin\Console\Commands\AdminMakeLanguage::class,
                \Admin\Console\Commands\AdminMakeCrud::class,
                \Admin\Console\Commands\AdminInit::class,
            ]);
        }

        // REGISTER ROUTES
        if (!$this->app->routesAreCached()) {
            $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        }

        // LOAD LANGUAGE AND VIEWS
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'admin');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'admin');

        // PUBLISH
        $this->registerPublishes();
    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/admin.php', 'admin');
        $this->mergeConfigFrom(__DIR__.'/../config/laratrust.php', 'laratrust');
        $this->mergeConfigFrom(__DIR__.'/../config/laratrust_seeder.php', 'laratrust_seeder');
    }


    /**
     * Register Publish Tags
     * @return void
     */
    public function registerPublishes()
    {
        $baseDir = __DIR__ . '/..';

        $date = date('Y_m_d_His');

        // PUBLISH SIDEBAR MENU
        $this->publishes([
            "$baseDir/resources/views/partials/sidebar.blade.php" => base_path('resources/views/vendor/admin/partials/sidebar.blade.php')
        ], 'menu');

        // PUBLISH ALL VIEWS
        $this->publishes([
            "$baseDir/resources/views" => base_path('resources/views/vendor/admin'),
            "$baseDir/resources/lang" => base_path('resources/lang/vendor/admin'),
        ], 'resources');

        // PUBLISH CONFIG FILES
        $this->publishes([
            "$baseDir/config/admin.php" => config_path('admin.php'),
            "$baseDir/config/laratrust.php" => config_path('laratrust.php'),
            "$baseDir/config/laratrust_seeder.php" => config_path('laratrust_seeder.php'),
        ], 'config');

        // PUBLISH PUBLIC ASSETS
        $this->publishes([
            "$baseDir/public/assets/dist" => public_path('vendor/admin/dist'),
            "$baseDir/public/assets/custom" => public_path('vendor/admin/custom'),
        ], 'public');

        // PUBLISH DATABASE SEEDS
        $this->publishes([
            "$baseDir/database/seeds/" => database_path('seeds'),
            "$baseDir/database/migrations/2015_10_12_200000_create_assets_table.php" => database_path('migrations/' . $date . '_create_assets_table.php'),
            "$baseDir/database/migrations/2015_10_12_200000_update_users_table.php" => database_path('migrations/' . $date . '_update_users_table.php'),
        ], 'database');
    }
}
