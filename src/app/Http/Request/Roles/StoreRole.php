<?php

namespace Admin\Http\Requests\Roles;

use Illuminate\Foundation\Http\FormRequest;

class StoreRole extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|unique:roles,name',
            'display_name' => 'required|string',
            'description' => 'string',
            'permissions' => 'array',
        ];

        if ($this->id) {
            $rules['name'] = $rules['name'] . ",$this->id";
        }

        return $rules;
    }
}
