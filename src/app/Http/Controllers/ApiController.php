<?php

namespace Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use Request;
use Response;
use App;

class ApiController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Api Controller
	|--------------------------------------------------------------------------
	|
	| Default methods for the API
	|
	*/
    // Set The Root for the view 'admin::' or 'admin.' for controller views
    public $viewPathRoot = '';

    public $headers = array();

    public function __construct() {

        function is_allowed() {

            $allowed_origins = array(
                'local' => array(
                    'chrome-extension://fdmmgilgnpjigdojojpjoooidkmcomcm',
                    '127.0.0.1',
                    'localhost'
                ),
                'lamp' => array(
                    'chrome-extension://fdmmgilgnpjigdojojpjoooidkmcomcm',
                    '127.0.0.1',
                    'localhost'
                ),
                'staging' => array(
                    'chrome-extension://fdmmgilgnpjigdojojpjoooidkmcomcm',
                    'http://stage.example.com',
                ),
                'production' => array(
                    'http://example.com',
                )
            );

            $string = Request::server('HTTP_ORIGIN');

            // remove port from end of variable if it's there
            $pos = strrpos($string, ':');
            if($pos > 6) {
                $string = substr($string, 0, $pos );
            }

            // default response
            $allowed = false;

            if ( in_array($string, $allowed_origins[App::environment()] )  ) {
                // do nothing, it's all good
                $allowed = true;
            } else {
                // not in array, sub string match
                foreach($allowed_origins[App::environment()] as $test) {

                    $strlen = strlen($string);
                    $testlen = strlen($test);
                    if ($testlen > $strlen) {
                        continue;
                    }
                    if (substr_compare($string, $test, -$testlen) === 0) {
                        $allowed = true;
                        break;
                    }

                }

            }

            return $allowed;

        }

        //
        // how can we do the above urls better and tie them into the URLS we already have listed in the bootstrap/start.php file
        //

        // do we want regualer or extended objects responses
        $this->extended = Request::input('extended', false);

        // headers
        // $this->headers['Access-Control-Allow-Origin'] = is_allowed() ? Request::server('HTTP_ORIGIN') : '*';
        $this->headers['Access-Control-Allow-Origin'] = '*';
        $this->headers['Access-Control-Allow-Methods'] = "GET, POST, PUT, PATCH, DELETE, OPTIONS";
        $this->headers['Access-Control-Allow-Headers'] = "Content-Type";
        $this->headers['Access-Control-Expose-Headers'] = "X-Pagination-Page, X-Pagination-Count, X-Pagination-Total-Objects, X-Pagination-Last-Page";
        $this->headers['Access-Control-Allow-Credentials'] = "true";

    }

    protected function respond($response_type = 'full') {

        // full or not, non 200 get short response
        if($response_type == 'full' and $this->response_code != 200) {
            $response_type = 'short';
        }

        if($response_type == 'full') {

            $data = [];

            if(isset($this->take)) {
                $data['take'] = (int) $this->take;
            }

            if(isset($this->skip)) {
                $data['skip'] = $this->skip;
            }

            if(isset($this->page)) {
                $data['page'] = $this->page;
            }

            $data['count'] = count($this->response);
            $data['data'] = $this->response;

        } else {

            $data = $this->response;

        }

        return Response::json(
            $data,
            $this->response_code,
            $this->headers
        )->setCallback(Request::input('callback'));

    }

    protected function validate_class($class) {

        $class = str_singular(ucfirst($class));

        if(strpos ($class, "." ) === FALSE) {

            if (!class_exists("App\\Models\\".$class)) {

                $this->response = array('error' => 'class_'.$class.'_not_found');
                $this->response_code = 404;

                return false;

            } else {

                $this->model = "\App\\Models\\".$class;

                return true;

            }

        } else {

            // this is a fail safe incase they try to access weird URL's
            // this feature was added in as a requirenment for a security scan
            return App::abort(404, 'Page not found');
            die;

        }

    }

 	/*
	|--------------------------------------------------------------------------
	| Index
	|--------------------------------------------------------------------------
	|
	| Responds to GET requests
	| Returns JSON
	|
	*/
	public function index()
    {

        if ($this->validate_class(Request::segment(2))) {

            // set pagination
            $total = new $this->model;
            $this->total = $total->get()->count();

            // set up custom pagination
            $this->take = Request::input('take', 30);
            $this->skip = Request::input('skip', 0);

            // use default pagination
            $this->page = Request::input('page', null);
            if($this->page) {
                $this->skip = $this->page * $this->take;
            }

            // set the last page based on the number of results
            if($this->take > $this->total) {
                $this->lastPage = 0;
            } else {
                $this->lastPage = ceil($this->total / $this->take);
            }

            // get the result
            $model = new $this->model;

            if ($this->extended) {
                // include extended objects
                $with = $model->extendedAttributesList();
            } else {
                // no extended objects
                $with = array();
            }

            $result = $model::api()->with($with)->take($this->take)->skip($this->skip)->get();

            // add additional headers for lists
            $this->headers['X-Pagination-Page'] = $this->page;
            $this->headers['X-Pagination-Total-Objects'] =$this->total;
            $this->headers['X-Pagination-Count'] = $this->take;
            $this->headers['X-Pagination-Last-Page'] = $this->lastPage;

            // set response
            $this->response = $result->toArray();
            $this->response_code = 200;

        }

        return $this->respond();

    }

	/*
	|--------------------------------------------------------------------------
	| Show - returns a specific object
	|--------------------------------------------------------------------------
	|
	| Responds to GET requests of the given ID
	| Returns JSON
	|
	*/
	public function show($id)
    {

        if ($this->validate_class(Request::segment(2))) {

            $this->take = 1;
            $this->skip = 0;

            // get the model to local variable
            $model = new $this->model;

            if ($this->extended) {
                // include extended objects
                $with = $model->extendedAttributes();
            } else {
                // no extended objects
                $with = array();
            }

            $result = \Cache::remember('apicontroller.show.'.$this->model.'.'.$id, \Config::get('site.cache_ttl'), function() use($model, $id)
            {
                return $model::with($model->extendedAttributes())->find($id);
            });

            if ($result) {

                $this->response = $result->toArray();
                $this->response_code = 200;

            } else {

                $this->response = array('error' => 'object_not_found');
                $this->response_code = 404;

            }

        }

        return $this->respond('short');

    }


 	/*
	|--------------------------------------------------------------------------
	| Related
	|--------------------------------------------------------------------------
	|
	| Responds to GET requests and returns the given model related to the first model given
	| URL: /api/MODEL/ID/RELATED_MODEL
	| Returns JSON
	|
	*/
	public function related($class, $id, $related_class)
    {

        if ($this->validate_class($class)) {

            // get the model to local variable
            $model = new $this->model;

            $relatedModel = $related_class;

            // get result
            $result = $model::find($id);

            if ($result) {

                if ($result->$relatedModel) {

                    $related = $result->$relatedModel;

                    $this->response = $result->$relatedModel->toArray();
                    $this->response_code = 200;

                } else {
                    // no related products, send empty array

                    $this->response = array();
                    $this->response_code = 200;

                }

            } else {

                $this->response = array('error' => 'object_not_found');
                $this->response_code = 404;

            }

        }

        return $this->respond();

    }


  	/*
	|--------------------------------------------------------------------------
	| Store
	|--------------------------------------------------------------------------
	|
	| Responds to POSTs of data to create/update a model
	| URL: /api/MODEL/store
	| Returns JSON
	|
	*/
	public function create($id = null) {

	    return $this->store();

	}

	public function update() {

    	return $this->store();

	}

	public function store($id = null) {

        if ($this->validate_class( Request::segment(2) )) {

            // get the model to local variable
            $model = new $this->model;

            if ($id = Request::input('id', $id)) {
                $model = $model::find($id);
                $success_code = 200;
            } else {
                $success_code = 201;
            }

            $validator_class = '\\App\\Services\\Validators\\'.$this->model;
            if(!class_exists($validator_class)) {
              $validator_class = '\\App\\Services\\Validators\\Generic';
            }
            $validator = new $validator_class();

            if ($validator->passes()) {

                $model->fill(Request::all());

                if ($model->save()) {

                    $model->load( $model->extendedAttributes() );

                    $this->response = $model->toArray();
                    $this->response_code = $success_code;

                } else {

                    $this->response = array('error' => isset($model->error) ? $model->error : 'save_failed');
                    $this->response_code = 404;

                }

            } else {

                // The given data did not pass validation
                $this->response = array('error' => $validator->messages() );
                $this->response_code = 422;

            }

        }

        return $this->respond();

	}


	public function destroy($id) {

        // get the model to local variable
        $model = new $this->model;

        // get the result
        $object = $model::find($id);

        if($object) {

            $object->delete();

            $this->response = array();
            $this->response_code = 200;

        } else {

            $this->response = array('error' => 'object_not_found');
            $this->response_code = 404;

        }

        return $this->respond();

	}


	public function relate($id, $relate_id) {

        // get the model to local variable
        $model = new $this->model;

        // get result
        $result = $model::find($id);

        if ($result) {

            // get object to relate it to
            $relatedModel = str_singular(ucfirst(Request::segment(4)));

            $related = new $relatedModel;
            $related = $related::find($relate_id);

            if ($related) {

                // whats the plural version to use in the save
            	$relationship = Request::segment(4);

                $result->$relationship()->save($related);

            	// response
                $this->response = array('success' => 'relationship_saved');
                $this->response_code = 201;

            } else {

                $this->response = array('error' => 'related_object_not_found');
                $this->response_code = 404;

            }

        } else {

            $this->response = array('error' => 'object_not_found');
            $this->response_code = 404;

        }

        return $this->respond();

	}


	public function unrelate($id, $relate_id) {

        // get the model to local variable
        $model = new $this->model;

        // get result
        $result = $model::find($id);

        // get object to relate it to
        $relatedModel = str_singular(ucfirst(Request::segment(4)));

        $related = new $relatedModel;
        $related = $related::find($relate_id);

        // whats the plural version to use in the save
    	$relationship = Request::segment(4);

        $result->$relationship()->detach($relate_id);

    	// response
        $this->response = array('success' => 'relationship_deleted');
        $this->response_code = 200;

        return $this->respond();

	}


	public function totalcount($class) {

        if ($this->validate_class($class)) {

            // get the model to local variable
            $model = new $this->model;

            // get the result
            $count = $model::count();

            if($count) {

                $this->response = array('count' => $count);
                $this->response_code = 200;

            } else {

                $this->response = array('count' => '0');
                $this->response_code = 404;

            }

        }

        return $this->respond();

	}



    /**
     * order
     * Pass a sorted array of id's to be arranged in the an order
     * @author Josh Hagel <josh@14four.com>
     * @return Array success
     */
    public function order() {

        $model = new $this->model();

        /** @var Array order id in order */
        $order = Request::input('order');
        /** @var intiger start position of sorted */
        $start = Request::input('start') || 0;

        $i = $start;
        foreach( $order as $id) {
            $slide = $model::find($id);
            $slide->order = $i;
            $slide->save();
            $i++;
        }

        return ['success' => true];
    }



	public function unauthorized() {

        $this->response = array('error' => 'authentication_required');
        $this->response_code = 401;

        return $this->respond();
	}

}
