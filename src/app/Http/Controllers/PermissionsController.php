<?php

namespace Admin\Http\Controllers;

use Config;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Admin\Http\Requests\Permissions\StorePermission;

class PermissionsController extends BaseController
{
    public $take = 30;

    private $roleModel;

    private $permissionModel;

    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->permissionModel = Config::get('laratrust.models.permission');

        $this->roleModel = Config::get('laratrust.models.role');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissionModel = new $this->permissionModel;
        $countModel = new $this->permissionModel;

        $permissions = $permissionModel::with('roles')->paginate($this->take);
        $count = $countModel::count();

        return view('admin::permissions.index')->with(compact('permissions', 'count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rolesModel = new $this->roleModel;

        $roles = $rolesModel->get();

        return view('admin::permissions.form')->with(compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePermission $request)
    {
        $permission = new $this->permissionModel;

        $permission->name = $request->input('name', '');
        $permission->display_name = $request->input('display_name', '');
        $permission->description = $request->input('description', '');

        $permission->save();

        $permission->roles()->sync($request->input('roles', []));

        return redirect()->route('admin.permissions.show', ['id' => $permission->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permissionModel = new $this->permissionModel;

        $permission = $permissionModel->with('roles')->findOrFail($id);

        return view('admin::permissions.show')->with(compact('permission', 'roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rolesModel = new $this->roleModel;
        $permissionModel = new $this->permissionModel;

        $roles = $rolesModel->get();
        $permission = $permissionModel->findOrFail($id);

        return view('admin::permissions.form')->with(compact('permission', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePermission $request, $id)
    {
        $permissionModel = new $this->permissionModel;

        $permission = $permissionModel->findOrFail($id);

        $permission->name = $request->input('name', '');
        $permission->display_name = $request->input('display_name', '');
        $permission->description = $request->input('description', '');

        $permission->roles()->sync($request->input('roles', []));

        $permission->save();

        return redirect()->route('admin.permissions.show', compact('id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permissionModel = new $this->permissionModel;

        $permission = $permissionModel::findOrFail($id);

        $displayName = $permission->display_name;

        $permission->delete();

        return redirect()->route('admin.permissions.index')->with('message', "$displayName was deleted!");
    }
}
