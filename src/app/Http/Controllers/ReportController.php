<?php

namespace Admin\Http\Controllers;

use Gate;
use Request;
use Auth;
use DB;
use Config;
use Admin\Helpers\Helpers;
use Admin\Models\User;

class ReportController extends AdminController
{

    public function dashboard()
    {
        $model = Request::segment(3);

        $data['model'] = $model;

        $total = DB::select('SELECT COUNT(id) as count FROM '.$model.';');
        $data['total'] = $total[0]->count;

        $l7 = DB::select('SELECT COUNT(*) as count FROM '.$model.' WHERE created_at between date_sub(now(), INTERVAL 1 WEEK) and now();');
        $data['last_seven_days'] = $l7[0]->count;

        $p7 = DB::select('SELECT COUNT(*) as count FROM '.$model.' WHERE created_at between date_sub(now(), INTERVAL 2 WEEK) and date_sub(now(), INTERVAL 1 WEEK);');
        $data['previous_seven_days'] = $p7[0]->count;

        $data['seven_days_difference'] =  Helpers::percent_change($l7[0]->count, $p7[0]->count);

        $l30 = DB::select('SELECT COUNT(*) as count FROM '.$model.' WHERE created_at between date_sub(now(), INTERVAL 1 MONTH) and now();');
        $data['last_thirty_days'] = $l30[0]->count;

        $p30 = DB::select('SELECT COUNT(*) as count FROM '.$model.' WHERE created_at between date_sub(now(), INTERVAL 2 MONTH) and date_sub(now(), INTERVAL 1 MONTH);');
        $data['previous_thirty_days'] = $p30[0]->count;

        $data['thirty_days_difference'] = Helpers::percent_change($l30[0]->count, $p30[0]->count);

        $data['by_week'] = DB::select('SELECT year, week, COUNT(*) as count FROM (SELECT YEAR(created_at) as year, WEEK(created_at) as week FROM '.$model.') sub group by year, week;');

        $data['dumb'] = Auth::user()->role;

        return view('admin::reports.index', $data);
    }


    public function export() {

        $model = Request::segment(3);

        $output = implode(",", array('BY WEEK', 'Count'));

        $data = DB::select('SELECT year, week, COUNT(*) as count FROM (SELECT YEAR(created_at) as year, WEEK(created_at) as week FROM '.$model.') sub group by year, week;');

        foreach ($data as $row) {
            $output .= "\r\n";
            // iterate over each tweet and add it to the csv
            $output .=  implode(",", [date('n/j/Y', strtotime($row->year . 'W' . str_pad($row->week, 2, '0', STR_PAD_LEFT)) ), $row->count]); // append each row
        }

        header('Content-Disposition: attachment; filename="export.csv"');
        header("Cache-control: private");
        header("Content-type: application/force-download");
        header("Content-transfer-encoding: binary\n");

        echo $output;

        exit;
    }

}
