<?php

namespace Admin\Http\Controllers;

use App;
use View;
use Redirect;
use Validator;
use Config;
use Schema;
use DB;
use Storage;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;

class AdminController extends BaseController
{

    use ValidatesRequests, DispatchesJobs;

    // Set The Root for the view 'admin::' or 'admin.' for controller views
    public $viewPathRoot = '';

    // @string Sets a model string from model to use (ex: 'App\\Models\\Users')
    public $model = null;

    // @integer Set number of Pages to Take in a single pull
    public $take = 30;

    /**
     * Set The filepath for the file accross storage
     * @var string  Defaults to
     */
    public $filePath = '';

    /**
     * Set the File Visability for the Storage
     * @var string Public Visability defaults to 'public' optional 'private'
     */
    public $fileVisability = 'public';

    /**
     * Gets the driver for the publc path of a file.
     * @var string  Defaults to the default driver for the system
     */
    public $filesystemsDriver;

    public $request;


    public function __construct(Request $request)
    {

        $this->request = $request;

        if ( $this->validate_class() ) {

            // get the total object count for this model
            $total = new $this->model();
            $this->total = $total->count();

            $this->setFilePath();
            $this->setStorageDriver();

        }

    }



    /**
     * [setFilePath description]
     */
    private function setFilePath() {

        if ( $this->filePath === '') {
            $this->filePath = $this->modelPlural . '/';
        }

        return true;
    }


    /**
     * [setStorageDriver description]
     */
    private function setStorageDriver() {

        if ( !$this->filesystemsDriver ) {
            $this->filesystemsDriver = Config::get('filesystems.default');
        }

        return true;
    }


    protected function validate_class()
    {

        $segment = $this->request->segment(2);

        // If custom Model is defined in the controller
        if (isset($this->model) && class_exists($this->model)) {
            $this->modelPlural = $segment;
            return true;
        }

        $class = str_singular(ucfirst($segment));

        if (strpos($class, '.') === false) {
            if (class_exists('App\\Models\\'.$class)) {
                $this->model = '\\App\\Models\\'.$class;
            }elseif (class_exists('Admin\\Models\\'.$class)) {
                $this->model = '\\Admin\\Models\\'.$class;
            } else {
                $this->response = array('error' => 'class_'.$class.'_not_found');
                $this->response_code = 404;
                return false;
            }

            $this->modelPlural = $segment;

            return true;
        } else {

            // this is a fail safe incase they try to access weird URL's
            // this feature was added in as a requirenment for a security scan
            return App::abort(404, 'Page not found');
            die;
        }
    }



    /**
     * Check View Route
     * This checks the view route to make sure that it is avaliable this will
     * redirect the user to a page that matches the view and makes sure there
     * is something to display.
     * @param String $view      of the viewname
     * @return String viewpath to use when rendering the veiw.
     */
    protected function getViewPath( $view ) {

        $route = $this->modelPlural;

        if ( $this->viewPathRoot === '' || !View::exists($this->viewPathRoot . $route . '.' . $view) ) {
            if ( View::exists('admin.' . $route . '.' . $view) ) {
                $this->viewPathRoot = 'admin.';
            } else if ( View::exists('admin::' . $route . '.' . $view) ) {
                $this->viewPathRoot = 'admin::';
            } else {
                $this->viewPathRoot = 'admin::';
                $route = 'dynamic';
                $this->setupDynamicView();
            }
        }

        return $this->viewPathRoot . $route . '.' . $view;

    }

    /**
     * Setup Dynamic View
     * @author Josh Hagel <josh@14four.com>
     * @return Bool true;
     */
    protected function setupDynamicView() {

        $model = new $this->model();
        $tableName = $model->getTableName();

        $columns = DB::select( DB::raw('SHOW COLUMNS FROM ' . $tableName));

        View::composer(['*'], function ($view) use ($columns, $model) {
            $view->with('pageDetails', [
                'name' => ucwords(str_replace('-', ' ', $this->modelPlural)),
                'route' => $this->request->segment(2),
                'model' => $this->modelPlural,
                'info' => $model,
                'columns' => $columns,
            ]);
        });

        return true;
    }


    /**
     * Index
     * @return View Return View for This Page
     */
    public function index( )
    {

        // get the model to local variable
        $model = new $this->model();

        // get the result
        $data[$this->modelPlural] = $model::filter( $this->request->all() )->order( $this->request->input('sort', null) )->paginate( $this->take );
        $data['count'] = $model::filter( $this->request->all() )->count();

        $childView = $this->getViewPath('index');

        return view($childView, $data);

    }


    public function create()
    {

        // get the result
        $data[$this->modelPlural] = new $this->model();

        $childView = $this->getViewPath('form');

        return view($childView, $data);
    }


    public function show($id)
    {

        if (isset(func_get_args()[1])) {
            $id = func_get_args()[1];
        }

        // get the model to local variable
        $model = new $this->model();

        // set extended objects
        // $with = $model->extendedAttributes();

        // get the result
        $object = $model::find($id);

        $data[$this->modelPlural] = $object;

        $childView = $this->getViewPath('show');

        return view($childView, $data);
    }


    public function edit($id)
    {
        if (isset(func_get_args()[1])) {
            $id = func_get_args()[1];
        }

        // get the model to local variable
        $model = new $this->model();

        // get the result
        $object = $model::find($id);

        $data[$this->modelPlural] = $object;

        $childView = $this->getViewPath('form');

        return view($childView, $data);
    }


    public function store( Request $request )
    {

        // get the model to local variable
        $model = new $this->model();

        if ($request->input('id')) {
            $model = $model::find($request->input('id'));
        }

        $input = $request->except('sync', 'redirect', 'related');

        $files = $request->allFiles();

        // start image upload
        foreach ( $files as $key => $file )  {
            if ( $file->isValid() ) {
                $filename = $file->getClientOriginalName();
                $filepath = $this->filePath . $filename;
                $content = file_get_contents( $file->getRealPath() );
                Storage::disk( $this->filesystemsDriver )->put( $filepath, $content, $this->fileVisability);
                $input[$key] = $filepath;
            }
        }

        // set up validator
        $validator = Validator::make($input, $model::$rules);

        if ($validator->passes()) {
            $model->fill($input);
            $model->save();

            $sync = $request->input('sync');

            if (!empty($sync)) {
                foreach ($sync as $key => $value) {
                    $key = str_plural($key);

                    $tosync = array();

                    foreach ($value as $key2 => $value2) {
                        $tosync[$value2['id']] = array('side' => $value2['side']);
                    }

                    $model->$key()->sync($tosync);
                }
            }

            // related objects
            $related = $request->input('related');

            if (!empty($related)) {
                foreach ($related as $key => $value) {
                    $key = ucfirst($key);

                    if (!empty($value['id'])) {
                        $sub_model = $key::find($value['id']);
                    } else {
                        $sub_model = new $key();
                    }

                    $sub_model->fill($value);
                    $sub_model->save();

                    $model->$key()->associate($sub_model);
                    $model->save();
                }
            }

            return Redirect::to( $request->input('redirect', Config::get('admin.uri') . '/' . $this->modelPlural) )->with('success', true)->with('reason', trans('admin::admin.saved'));
        } else {
            return Redirect::to( Config::get('admin.uri') . '/' . $this->modelPlural . '/create' )->withInput()->withErrors($validator->messages());
        }
    }




    public function update( Request $request, $id) {

        $modelInsance = new $this->model;

        $model = $modelInsance->findOrFail( $id );

        $this->validate( $request, $model->rules(), $model->messages() );

    }




    public function delete($id) {
        if (isset(func_get_args()[1])) {
            $id = func_get_args()[1];
        }

        $this->destroy($id);
    }

    public function destroy($id)
    {

        if (isset(func_get_args()[1])) {
            $id = func_get_args()[1];
        }

        // get the model to local variable
        $model = new $this->model();


        // get the result
        $object = $model::find($id);

        if (!$object) {
            return Redirect::to( Config::get('admin.uri') . '/' . $this->modelPlural )->with('success', trans('admin::admin.deleted'));
        }

        $object->delete();

        return Redirect::to( Config::get('admin.uri') . '/' . $this->modelPlural )->with('success', trans('admin::admin.deleted'));
    }
}
