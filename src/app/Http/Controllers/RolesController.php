<?php

namespace Admin\Http\Controllers;

use Config;
use Admin\Http\Requests\Roles\StoreRole;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class RolesController extends BaseController
{
    public $take = 30;

    private $roleModel;

    private $permissionModel;

    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->take = $this->request->input('take', $this->take);

        $this->permissionModel = Config::get('laratrust.models.permission');

        $this->roleModel = Config::get('laratrust.models.role');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roleModel = new $this->roleModel;

        $roles = $roleModel::with('permissions')->paginate($this->take);
        $count = $roleModel::count();

        return view('admin::roles.index')->with(compact('roles', 'count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissionsModel = $this->permissionModel;

        $permissions = $permissionsModel::get();

        return view('admin::roles.form')->with(compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRole $request)
    {
        $role = new $this->roleModel;

        $role->name = $request->input('name', '');
        $role->display_name = $request->input('display_name', '');
        $role->description = $request->input('description', '');

        $role->save();

        $role->permissions()->sync($request->input('permissions', []));

        return redirect()->route('admin.roles.show', ['id' => $role->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $roleModel = new $this->roleModel;

        $role = $roleModel::with('permissions')->findOrFail($id);

        return view('admin::roles.show')->with(compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rolesModel = new $this->roleModel;
        $permissionModel = new $this->permissionModel;

        $role = $rolesModel->findOrFail($id);
        $permissions = $permissionModel->get();

        return view('admin::roles.form')->with(compact('permissions', 'role'));
    }

    /**
     * [update description]
     * @param StoreRole $request [description]
     * @param [type]    $id      [description]
     * @return [type]    [description]
     */
    public function update(StoreRole $request, $id)
    {
        $roleModel = new $this->roleModel;

        $role = $roleModel->findOrFail($id);

        $role->name = $request->input('name', '');
        $role->display_name = $request->input('display_name', '');
        $role->description = $request->input('description', '');

        $role->permissions()->sync($request->input('permissions', []));

        $role->save();

        return redirect()->route('admin.roles.show', compact('id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $roleModel = new $this->roleModel;

        $role = $roleModel->findOrFail($id);

        $displayName = $role->display_name;

        $role->delete();

        return redirect()->route('admin.roles.index')->with('message', "$displayName was deleted!");
    }
}
