<?php

namespace Admin\Http\Controllers;

use Auth;
use Gate;
use Request;
use Config;
use App\Models\User;
use Illuminate\Support\Facades\File;

class ToolController extends AdminController
{

    public function database()
    {
        return view('admin::tools.database');
    }

    public function server()
    {
        $extensions = [];
        $extensions['curl'] = extension_loaded('curl') ? 'Yes' : 'No';
        $extensions['gd'] = extension_loaded('gd') ? 'Yes' : 'No';
        $extensions['json'] = extension_loaded('json') ? 'Yes' : 'No';
        $extensions['pdo'] = extension_loaded('pdo') ? 'Yes' : 'No';
        $extensions['mycrypt'] = extension_loaded('mcrypt') ? 'Yes' : 'No';
        $extensions['mysql'] = extension_loaded('mysql') ? 'Yes' : 'No';
        if (function_exists('apache_get_modules')) {
            $extensions['mod_rewrite'] = in_array('mod_rewrite', apache_get_modules()) ? 'Yes' : 'No';
        } else {
            $extensions['mod_rewrite'] = '';
        }

        $data['extensions'] = $extensions;
        $data['dumb'] = Auth::user()->role;

        return view('admin::tools.server', $data);
    }

    public function logs()
    {
        $data['file'] = storage_path('logs/laravel.log');
        $data['log'] = [];
        $data['dumb'] = Auth::user()->role;

        try{
            $lines = file($data['file']);
        }catch (Exception $exception){
            return view('admin::tools.log', $data);
        }

        $error = '';
        $trace = [];

        foreach ($lines as $line_num => $line) {
            if (substr($line, 0, 1) == '[') {
                if (!empty($error)) {
                    // add to parent array and reset for next loop
                    $foo = array(
                        'error' => $error,
                        'trace' => $trace,
                    );
                    $data['log'][] = $foo;

                    // reset
                    $error = '';
                    $trace = [];
                }

                // add new error
                $error = $line;
            } elseif (substr($line, 0, 5) == 'Stack') {
                // do nothing
            } elseif (substr($line, 0, 1) == '#') {
                // add to trace
                $trace[] = $line;
            }
        }


        return view('admin::tools.log', $data);
    }

    public function log_files()
    {

        $log_files = [];

        //Directories in which we want to collect log files from.
        $log_directories = [
            'logs' => storage_path('logs')
        ];

        foreach($log_directories as $dir_alias => $log_directory){
            if (!File::isDirectory($log_directory)) {
                continue;
            }

            foreach(File::allFiles($log_directory) as $log_file){
                $log_files[$dir_alias.'/'.$log_file->filename] = $log_directory.'/'.$log_file->filename;
            }
        }

        dd($log_files);

    }
}
