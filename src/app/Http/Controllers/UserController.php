<?php

namespace Admin\Http\Controllers;

use Password;
use Validator;
use Hash;
use Redirect;
use Session;
use Config;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Auth\Passwords\PasswordBroker;
use Admin\Http\Controllers\AdminController;
use Gate;

class UserController extends AdminController
{
    use ResetsPasswords;

    public $take = 30;

    public $request;

    public $userModel;

    public $roleModel;


    /**
     *
     */
    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->userModel = Config::get('auth.providers.users.model');

        $this->roleModel = Config::get('laratrust.models.role');
    }


    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
        $userModel = new $this->userModel;
        $countModel = new $this->userModel;

        // get the result
        $users = $userModel::filter($this->request->all())->order($this->request->input('sort', null))->paginate($this->take);
        $count = $countModel::filter($this->request->all())->count();

        return view('admin::users.index')->with(compact('users', 'count'));
    }


    public function show($id)
    {
        $userModel = new $this->userModel;

        $user = $userModel->findOrFail($id);

        return view('admin::users.show')->with(compact('user'));
    }


    /**
     * [create description]
     * @return [type] [description]
     */
    public function create()
    {
        $roleModel = new $this->roleModel;

        $roles = $roleModel::get();

        return view('admin::users.form')->with(compact('roles'));
    }


    /**
     * Show edit form
     * @param [type] $id [description]
     * @return [type] [description]
     */
    public function edit($id)
    {
        $userModel = new $this->userModel;
        $roleModel = new $this->roleModel;

        $user = $userModel::with('roles')->findOrFail($id);

        $roles = $roleModel::get();

        return view('admin::users.form')->with(compact('user', 'roles'));
    }



    /**
     * [store description].
     *
     * @return [type] [description]
     */
    public function store(Request $request)
    {
        $user = new $this->userModel;

        $rules = [
            'password' => 'required|confirmed',
        ];

        $this->validate($request, array_merge($user->rules(), $rules), $user->messages());

        $user->fill($request->except(['password', 'password_confirmation', 'role']));

        $user->password = Hash::make($request->password);

        $user->save();

        if ($request->has('role')) {
            $roles = $request->input('role', []);

            foreach ($roles as $role) {
                $user->roles()->attach((int)$role);
            }

            $user->save();
        }

        return Redirect::to($request->input('redirect', Config::get('admin.uri') . "/users/$user->id"))->with('success', true)->with('message', trans('admin::admin.created'));
    }



    public function update(Request $request, $id)
    {
        $userModel = new $this->userModel;

        $rules = [];

        $user = $userModel::findOrFail($id);

        if ($request->password) {
            $rules = [
                'password' => 'required|confirmed',
            ];
        }

        $this->validate($request, array_merge($user->rules(), $rules), $user->messages());

        $user->fill($request->except(['password', 'password_confirmation', 'role']));

        if ($request->input('password')) {
            $user->password = Hash::make($request->input('password'));
        }

        if ($request->has('role')) {
            $roles = $request->input('role', []);
            $user->roles()->sync([]);
            foreach ($roles as $role) {
                $user->roles()->attach((int)$role);
            }
        }

        $user->save();

        return Redirect::to($request->input('redirect', Config::get('admin.uri') . "/users/$user->id"))->with('success', true)->with('message', trans('admin::admin.saved'));
    }


    public function forgot($id, PasswordBroker $passwords)
    {
        $user = User::find($id);

        $credentials = ['id' => $user->id, 'email' => $user->email];

        $passwords->subject = trans('admin::users.messages.forgot_subject');

        $response = $passwords->sendResetLink($credentials, function ($message) {
            $message->subject(trans('admin::users.messages.forgot_subject'));
        });

        switch ($response) {
            case PasswordBroker::RESET_LINK_SENT:
                return Redirect::to(Config::get('admin.uri') . '/users/' . $user->id)->with('success', true)->with('message', trans('admin::users.messages.forgot_sent'));

            case PasswordBroker::INVALID_USER:
                return Redirect::to(Config::get('admin.uri') . '/users/' . $user->id)->with('error', true)->with('message', trans('admin::users.messages.forgot_error'));
        }
    }



    public function destroy($id)
    {
        $userModel = new $this->userModel;

        $user = $userModel::findOrFail($id);

        $user->delete();

        return Redirect::to(Config::get('admin.uri') . '/users')->with('success', trans('admin::admin.deleted'));
    }
}
