<?php

namespace Admin\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Config;

class LoginController extends Controller
{
    public $decayMinutes = 10;
    public $maxAttempts = 5;

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->decayMinutes = Config::get('admin.account_lockout.reenable'); // minutes to lockout
        $this->maxAttempts = Config::get('admin.account_lockout.attempts'); // attempts to lockout
        $this->middleware('guest')->except('logout');
    }

    /**
     * Set the redirect to location
     * @return string
     */
    protected function redirectTo()
    {
        return '/' . Config::get('admin.uri');
    }

    /**
     * Show Login Form
     * @return View Auth Login View
     */
    public function showLoginForm()
    {
        return view('admin::auth.login');
    }
}
