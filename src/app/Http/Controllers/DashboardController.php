<?php

namespace Admin\Http\Controllers;

use Gate;
use Config;
use DB;
use Carbon\Carbon;

class DashboardController extends AdminController
{
    public function index()
    {

        $user = Config::get('auth.providers.users.model');
        // dd(new $user);
        $users = DB::table('users')
            ->select( DB::raw('count(*) as count, created_at') )
            // ->groupBy( DB::raw('DATE_FORMAT(`created_at`, "%Y%m%d")') )
            ->groupBy( 'created_at' )
            ->orderBy( 'created_at' )
            ->get();
        $usersTotal = $user::count();

        $userCount = [];
        $userLabels = [];

        // dd($users);

        foreach( $users as $user ) {
            $userCount[] = $user->count;
            $userLabels[] = $user->count . ' | ' . Carbon::parse( $user->created_at )->format('m/d');
        }

        return view('admin::dashboard.index', ['userCount' => $userCount, 'userLabels' => $userLabels, 'usersTotal' => $usersTotal]);

    }
}
