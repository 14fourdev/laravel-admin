<?php
namespace Admin\Contracts;

interface Generator {

    public function getTemplates();

    public function getOutputPath();

    public function getTemplatePaths();

    public function getOutputName( $name );

    public function getData();

}
