<?php
namespace Admin\Contracts;

interface Searchable {

    public function apiSearchable();

    public function scopeOrder($query, $sort);

    public function scopeFilter($query, $terms);

    public function scopeFilter($query, $terms);

}
