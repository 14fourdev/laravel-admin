<?php namespace Admin\Helpers;

use Illuminate\Filesystem\Filesystem as File;

class StubProcess {

    public $files;

    public $data;

    public $destination;

    private $types = [
        'blade.php',
        'scss',
        'js'
    ];

    public function __construct( $data, $destination ) {

        $this->files = new File;

        $this->setData( $data );

        $this->setDestination( $destination );

    }

    public function setData( $data = [] ) {

        $this->data = $data;

    }

    public function setDestination( $destination ) {

        $this->destination = $destination;

    }

    public function processFiles( $directory ) {

        $files = $this->files->allFiles( $directory );

        foreach( $files as $file ) {
            $this->processFile( $file );
        }

        return true;

    }

    public function processFile( $file, $fileName = false ) {

        if ( !$fileName ) {
            $fileName = $this->getFileName( $file );
        }

        $type = $this->getFileType( $fileName );

        $content = $this->getFileContent( $file->getPathName() );

        $this->makeDir( $this->destination . $file->getRelativePath() );

        $this->putFile( $this->destination  . $file->getRelativePath() . '/' . $fileName, $content );

    }

    private function getFileName( $file ) {

        $fileName = substr($file->getFileName(), 0, strpos($file->getFileName(), ".stub"));

        return $fileName;

    }

    private function getFileType( $name ) {
        foreach( $this->types as $type )
        {
            if (strpos($name, $type) !== FALSE) {
                return $type;
            }
        }
        return '';
    }

    private function getFileContent( $path ) {
        $stub = $this->files->get( $path );
        $content = $this->replaceData( $stub );
        return $content;
    }


    /**
     * Replace Data
     *
     * Match array of data and returns the updated content
     *
     * @String
     * return @String
     */
    private function replaceData( $stub ) {

        $content = $stub;

        foreach ( $this->data as $key => $value ) {
            $content = $this->replace($content, $value, $key);
        }

        return $content;
    }


    private function replace($stub, $value, $option) {
        return str_replace('{{' . $option . '}}', $value, $stub);
    }

    private function putFile( $file, $cotent ) {
        $this->files->put($file, $cotent);
    }

    private function makeDir( $directory ) {
        if ( !$this->files->isDirectory( $directory ) ) {
            $this->files->makeDirectory( $directory, 0777, true);
        }
    }
}
