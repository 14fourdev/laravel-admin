<?php
namespace Admin\Helpers;


class Helpers {

  /*
  |--------------------------------------------------------------------------
  | Percent Change
  |--------------------------------------------------------------------------
  |
  | Returns a percent based on the two given numbers
  |
  */

  public static function percent_change($new_value, $old_value, $decimal = 0) {
      if ($old_value > 0) {
          return number_format(($new_value - $old_value) / $old_value * 100, $decimal);
      } else {
          return $new_value * 100;
      }
  }


  /*
  |--------------------------------------------------------------------------
  | Rand String
  |--------------------------------------------------------------------------
  |
  | Returns a random string
  |
  */

  public static function random_string ($length) {
      $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      return substr(str_shuffle($chars),0,$length);
  }


  /*
  |--------------------------------------------------------------------------
  | String To URL
  |--------------------------------------------------------------------------
  |
  | Formats a string to be used as a URL slug
  |
  */

  public static function str_to_url($value) {
      return strtolower(str_replace(' ', '-', str_replace("'", '', str_replace("?", '', $value))));
  }


  /*
  |--------------------------------------------------------------------------
  | DB Close Conenction
  |--------------------------------------------------------------------------
  |
  | Closes DB connections so they dont persist and clog down the DB
  | https://www.drupal.org/node/843114#comment-6075722
  | the disconnect appeard to not work unless I used the drupal hack fix above.
  | but it seems commenting out these lines did not automatically reintroduce the bug
  | I dont really know, so I'm going to leave it here and see if it fixes the issue
  |
  */

  public static function db_close_connection() {
      $pdo = DB::connection()->getPdo();
      $pdo->setAttribute(PDO::ATTR_PERSISTENT, false);
      DB::disconnect();
  }


  /*
  |--------------------------------------------------------------------------
  | Date To Age
  |--------------------------------------------------------------------------
  |
  | Calculates the age from the given date
  |
  */

  public static function date_to_age($date) {

      $time = time(); // Store time for consistency

      if (isset($date) && !empty($date)) {

          $date = strtotime($date);

          $day = 60 * 60 * 24;
          $year = $day * 365;

          $thisYear = date("Y", $time);

          $numYears = $thisYear - date('Y', $date);
          $leapYears = $numYears / 4 * $day; // Calculate for accuracy

          $ageTime = $date;
          $age = $time - $ageTime - $leapYears;

          return floor($age / $year);

      } else {

          return false; // No birthdate specified

      }

  }

}
