<?php


/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the Admin.
|
*/

Route::group([
        'domain' => Config::get('admin.domain'),
        'prefix' => Config::get('admin.uri'),
        'namespace' => 'Admin\Http\Controllers',
        'middleware' => ['web'],
    ], function() {
        Route::group(['middleware' => 'guest'], function () {
            Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
            Route::post('login', 'Auth\LoginController@login');

            // FORGOT PASSWORD
            Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
            Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');

            // PASSWORD RESET
            Route::post('password/reset', 'Auth\ResetPasswordController@reset');
            Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');

            // REGISTRATION
            Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('admin.register');
            Route::post('register', 'Auth\RegisterController@register');
        });

        // LOGOUT
        Route::get('logout', 'Auth\LoginController@logout');
        Route::post('logout', 'Auth\LoginController@logout')->name('logout');


        Route::group([
                'middleware' => ['permission:admin.*']
            ], function () {
                Route::get('/', 'DashboardController@index')->name('admin.dashboard');

                // Users
                Route::get('/users', 'UserController@index')->middleware('permission:admin.users.read')->name('admin.users.index');
                Route::post('/users', 'UserController@store')->middleware('permission:admin.users.create')->name('admin.users.store');
                Route::get('/users/create', 'UserController@create')->middleware('permission:admin.users.create')->name('admin.users.create');
                Route::get('/users/{user}', 'UserController@show')->middleware('permission:admin.users.read')->name('admin.users.show');
                Route::patch('/users/{user}', 'UserController@update')->middleware('permission:admin.users.update')->name('admin.users.update');
                Route::delete('/users/{user}', 'UserController@destroy')->middleware('permission:admin.users.delete')->name('admin.users.destroy');
                Route::get('/users/{user}/edit', 'UserController@edit')->middleware('permission:admin.users.update')->name('admin.users.update');
                Route::post('/users/{user}/forgot', 'UserController@forgot')->middleware('permission:admin.users.update')->name('admin.users.forgot');

                // ACCESS CONTROL
                Route::group(['prefix' => 'access-control'], function () {
                    // PERMISSIONS
                    Route::get('/permissions', 'PermissionsController@index')->middleware('permission:admin.acl.read')->name('admin.permissions.index');
                    Route::post('/permissions', 'PermissionsController@store')->middleware('permission:admin.acl.create')->name('admin.permissions.store');
                    Route::get('/permissions/create', 'PermissionsController@create')->middleware('permission:admin.acl.create')->name('admin.permissions.create');
                    Route::get('/permissions/{user}', 'PermissionsController@show')->middleware('permission:admin.acl.read')->name('admin.permissions.show');
                    Route::patch('/permissions/{user}', 'PermissionsController@update')->middleware('permission:admin.acl.update')->name('admin.permissions.update');
                    Route::delete('/permissions/{user}', 'PermissionsController@destroy')->middleware('permission:admin.acl.delete')->name('admin.permissions.destroy');
                    Route::get('/permissions/{user}/edit', 'PermissionsController@edit')->middleware('permission:admin.acl.update')->name('admin.permissions.edit');

                    // ROLES
                    Route::get('/roles', 'RolesController@index')->middleware('permission:admin.acl.read')->name('admin.roles.index');
                    Route::post('/roles', 'RolesController@store')->middleware('permission:admin.acl.create')->name('admin.roles.store');
                    Route::get('/roles/create', 'RolesController@create')->middleware('permission:admin.acl.create')->name('admin.roles.create');
                    Route::get('/roles/{user}', 'RolesController@show')->middleware('permission:admin.acl.read')->name('admin.roles.show');
                    Route::patch('/roles/{user}', 'RolesController@update')->middleware('permission:admin.acl.update')->name('admin.roles.update');
                    Route::delete('/roles/{user}', 'RolesController@destroy')->middleware('permission:admin.acl.delete')->name('admin.roles.destroy');
                    Route::get('/roles/{user}/edit', 'RolesController@edit')->middleware('permission:admin.acl.update')->name('admin.roles.edit');
                });

                // Tools
                Route::group(['middleware' => ['permission:admin.tools.read']], function () {
                    Route::get('/tools/logs', 'ToolController@logs');
                    Route::get('/tools/logs/files', 'ToolController@log_files');
                    Route::get('/tools/server', 'ToolController@server');
                });

                // Reports
                Route::group(['middleware' => ['permission:admin.reports.read']], function () {
                    Route::get('/reports/users', 'ReportController@dashboard');
                    Route::get('/reports/users/export', 'ReportController@export');
                });
            }
        );

    }
);

