<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Update Users Table
 */
class UpdateUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('name');
        });

        Schema::table('users', function (Blueprint $table) {

            // CHANGE NAME TO FIRST | LAST
            $table->string('first_name', 100)->nullable();
            $table->string('last_name', 100)->nullable();

            // ADD USER INFROAMTION
            $table->string('address', 255)->nullable();
            $table->string('city', 255)->nullable();
            $table->string('state', 50)->nullable();
            $table->string('zipcode')->nullable();
            $table->string('phone', 50)->nullable();
            $table->date('birthdate')->nullable();

            // ADD CONFIRMATION AND ACTIVATION
            $table->boolean('confirmed')->default(0);
            $table->text('confirmation_code')->nullable();
            $table->boolean('active')->default(1);

            // PASSWORD PROTECTIONS
            $table->integer('attempts')->after('remember_token')->default(0);
            $table->text('used_passwords')->after('attempts')->nullable();
            $table->datetime('password_updated_at')->after('used_passwords')->nullable();

            // ICON
            $table->integer('asset_id')->nullable();

            // SOFT DELETES
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $columns = [
            'first_name',
            'last_name',
            'address',
            'city',
            'state',
            'zipcode',
            'phone',
            'birthdate',
            'confirmed',
            'confirmation_code',
            'active',
            'attempts',
            'used_passwords',
            'password_updated_at',
        ];

        foreach ($columns as $column) {
            Schema::table('users', function (Blueprint $table) use ($column) {
                $table->dropColumn($column);
            });
        }

        Schema::table('users', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('name')->nullable();
        });
    }
}
